# -----------------------------------------------------------------------------
# fove.py
#
# A small prover for first-order specifications.
# -----------------------------------------------------------------------------

import sys #For file opening and use of ply
sys.path.insert(0,"ply-3.8")
from z3 import * #For z3 proofs

if sys.version_info[0] >= 3:
    raw_input = input

reserved = {
    'Pre' : 'PRE', #keyword introducing precondition
    'Post': 'POST', #keyword introducing postcondition
    'add' : 'ADD', #keyword for node and edge label additions
    'del' : 'DEL' #keyword for node and edge label deletions
}

tokens = [
    'NAME', #String constants used for nominals, rule names, atomic labels
    'IMP', #Implication arrow =>
    'EQU', #Equivalence arrow <=>
    'RAR', #Delimiter for rules ->
    'INT'  #Integer
    ] + list(reserved.values())

literals = [
    ',', #rule delimiter
    '#', #forall symbol
    '^', #verum
    '@', #exists symbol
    '.', #delimiter for quantifiers
    '(', #usual parenthesis use
    ')', #usual parenthesis use
    '&', #conjunction in formulae
    '|', #disjunction in formulae
    ':', #delimiter between names and definitions
    '{', #rule left-hand side and strategy invariant open
    '}', #rule left-hand side and strategy invariant close
    ';', #strategy sequence
    '+', #strategy choice
    '*', #strategy iteration
    '~', #formula negation
    '<', #lesser than used in number comparisons
    '>', #greater than used in number comparisons
    '='  #number equality
]

# Tokens

def t_NAME(t):     #String constants used for nominals, rule names, atomic labels
    r'[a-zA-Z_][a-zA-Z0-9_]*'
    t.type = reserved.get(t.value,'NAME')    # Check for reserved words
    return t
    
t_IMP     = r'=>'   #Implication arrow
t_EQU     = r'<=>'  #Equivalence arrow
t_RAR     = r'->'   #Delimiter for rules

t_ignore = " \t"    #Ignore tabs and spaces

t_INT     = r'[1-9][0-9]*' #integers

def t_newline(t): #newline
    r'\n+'
    
def t_error(t): #error on unknown character
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)
    
# Build the lexer
import ply.lex as lex
lex.lex()

# Parsing rules

precedence = (('right','@','#'),('left','IMP','EQU','*'),('left','&','|','+',';'),('right','~'),)

# dictionary of rules
rules = {}
# dictionary of unary predicates
upred = {}
# dictionary of binary predicates
bpred = {}
# dictionary of vars
var = {}


class Tree: #class holding rules and formulae
    def __init__(self, cargo, t, left=None, right=None,center=None):
    	self.t = t
        self.cargo = cargo
        self.left  = left
        self.right = right
        self.center = center

    def __str__(self):
    	if self.t == 0: #binary prop & concept subst
    		return str(self.cargo) + "(" + str(self.left) + "," + str(self.right) + ")"
    	elif self.t == 1: #unary prop
    		return str(self.cargo) + "(" + str(self.left) + ")"
    	elif self.t == 2: #binary form
    		return str(self.left) + str(self.cargo) + str(self.right)
    	elif self.t == 3: #parenthesized form & strat
    		return "(" + str(self.cargo) + ")"
    	elif self.t == 4: #quantifier
    		return str(self.cargo) + str(self.left) + "." + str(self.right)
    	elif self.t == 5: #negation form
    		return "~" + str(self.cargo)	
    	elif self.t == 6: #closure strat
    		return str(self.cargo) + "*{" + str(self.left) + "}"
    	elif self.t == 7: #name strat & ^
    		return str(self.cargo)
    	elif self.t == 8: #subst form
    		return str(self.left) + "[" + str(self.cargo) + "]"
    	elif self.t == 9: #role subst
    		return str(self.cargo) + "(" + str(self.left) + "," + str(self.right) + "," + self.center + ")"
    	elif self.t == 10: #value comparison
    		return str(self.cargo) + " " + str(self.left) + " " + str(self.right)
    	elif self.t == 11: #var
    		return self.cargo
    	elif self.t == 12: #integer
    		return str(self.cargo)

def p_statement_qmark(p):
	'statement : PRE ":" expr rulelist strat POST ":" expr'
	print("Pre:" + str(p[3]))
	print("Strat:" + str(p[5]))
	print("Post:" + str(p[8]))
	wp = wp_s(p[5],p[8])
	print("wp:" + str(wp))
	vec = vc(p[5],p[8])
	print("vc: " + str(vec))
	corr = Tree("&",2,Tree("=>",2,p[3],supp_sub(wp)),supp_sub(vec))
	print("Corr: " + str(corr))
	
	s = Solver()
	
	#s.add(var['x'] == var['y'])
	#s.add(Exists([x],Not(f(x))))
	#t = Tree("=",0,'x','y')#],Tree("&",2,Tree('Q',0,'x','y'),Tree("1",7)))
	#print(Not(ToZ3(corr)))
	s.add(Not(ToZ3(corr)))
	#print(Not(Implies(Not(Exists([var['a']],And(upred['Pa'](var['a']),Not(upred['Per'](var['a'])),Not(upred['Pch'](var['a'])))))
	#	,ForAll([var['x']],Implies(upred['Pa'](var['x']),upred['Per'](var['x']))))))
	#s.add(And(val(var['x']) == 0,val(var['y']) > 1))
	#print(And(val(var['x']) == 0,val(var['y']) > 1))
	#print(s.check())
	if s.check().r == Z3_L_TRUE:
		print(s.model())
	else:
		print("unsat")
	#print(t)
	#print(upred)
	#print(bpred)
	#print(var)

def p_expr_bin(p):
	'''expr : expr "&" expr
			| expr "|" expr
			| expr IMP expr
			| expr EQU expr'''
	p[0] = Tree(p[2],2,p[1],p[3])

def p_expr_quantifier(p):
	'''expr : '#' listvar '.' expr
		| '@' listvar '.' expr'''
	p[0] = Tree(p[1],4,p[2],p[4])
	

def p_expr_par(p):
	'''expr : "(" expr ")"'''
	p[0] = Tree(p[2],3)

def p_expr_neg(p):
	'''expr : "~" expr'''
	p[0] = Tree(p[2],5)

def p_expr_concept(p):
	'''expr : NAME "(" NAME ")"'''
	p[0] = Tree(p[1],1,p[3])
	upred[p[1]] = Function(p[1],Node,BoolSort())
	var[p[3]] = Const(p[3],Node)

def p_expr_role(p):
	'''expr : NAME "(" NAME "," NAME ")"'''
	p[0] = Tree(p[1],0,p[3],p[5])
	bpred[p[1]] = Function(p[1],Node,Node,BoolSort())
	var[p[3]] = Const(p[3],Node)
	var[p[5]] = Const(p[5],Node)

def p_expr_val(p):
	'''expr : val comp val'''
	p[0] = Tree(p[1],10,p[2],p[3]) 

def p_val_node(p):
	'''val : NAME'''
	p[0] = Tree(p[1],11)
	var[p[1]] = Const(p[1],Node)

def p_val_int(p):
	'''val : INT'''
	p[0] = Tree(p[1],12)

def p_comp(p):
	'''comp : "<"
			| ">"
			| "="'''
	p[0] = p[1]

def p_expr_top(p):
	'''expr : "^"'''
	p[0] = Tree(p[1],7)

def p_listvar_atom(p):
	'''listvar : NAME'''
	p[0] = [p[1]]
	
def p_listvar_conc(p):
	'''listvar : NAME ',' listvar'''
	p[0] = [p[1]] + p[3]
	
def p_rulelist_simp(p):
	'''rulelist : rule'''
	#print('rulelist -> rule')

def p_rulelist_comp(p):
	'''rulelist : rule "," rulelist'''
	#print('rulelist -> rule ; rulelist')
	
def p_rule(p):
	'''rule : NAME ":" graph RAR listaction'''
	rules[p[1]] = (p[3],p[5])

def p_graph(p):
	'''graph : "{" nodenamelist ';' expr "}"'''
	p[0] = Tree("@",4,p[2],p[4])
	
def p_nodenamelist_simple(p):
	'''nodenamelist : NAME'''
	p[0] = [p[1]]
	var[p[1]] = Const(p[1],Node)
	
def p_nodenamelist_comp(p):
	'''nodenamelist : NAME "," nodenamelist'''	
	p[0] = [p[1]] + p[3]
	var[p[1]] = Const(p[1],Node)
	
def p_listaction_simp(p):
	'''listaction : action'''
	p[0] = [p[1]]
	
def p_listaction_comp(p):
	'''listaction : action ";" listaction'''
	p[0] = [p[1]] + p[3]
	
def p_action_conc(p):	
	'''action : act "(" NAME "," NAME ")"'''
	p[0] = Tree(p[1],0,p[3],p[5])
	var[p[1]] = Const(p[5],Node)
	upred[p[3]] = Function(p[3],Node,BoolSort())
	
def p_action_role(p):	
	'''action : act "(" NAME "," NAME "," NAME ")"'''
	p[0] = Tree(p[1],9,p[3],p[5],p[7])
	var[p[1]] = Const(p[5],Node)
	var[p[1]] = Const(p[7],Node)
	bpred[p[3]] = Function(p[3],Node,Node,BoolSort())
	
def p_act(p):
	'''act : ADD
		   | DEL'''
	p[0] = p[1]
	
def p_strat_atom(p):
	'''strat : NAME'''
	p[0] = Tree(p[1],7)
	
def p_strat_bin(p):
	'''strat : strat ";" strat
			 | strat "+" strat'''
	p[0] = Tree(p[2], 2, p[1], p[3])
	
def p_strat_mon(p):
	'''strat : strat "*" "{" expr "}"'''
	p[0] = Tree(p[1],6,p[4])
	
def p_strat_par(p):
	'''strat : "(" strat ")"'''
	p[0] = Tree(p[2],3)	
	
def wp_s(s,p):
	#if s.t == 0: #binary prop & concept subst
    #if s.t == 1: #unary prop
	if s.t == 2: #binary strat: either concat or choice
		if s.cargo == ";": #concat
			return wp_s(s.left,wp_s(s.right,p))
		elif s.cargo == "+": #choice
			return Tree("&",2,wp_s(s.left,p),wp_s(s.right,p))
	elif s.t == 3: #parenthesized strat
		return wp_s(s.cargo,p)
	#if s.t == 4: #quantifier
    #if s.t == 5: #negation form
	elif s.t == 6: #closure
            print s.left
	    return s.left
	elif s.t == 7: #simple rule application
		return Tree("@",4,rules[s.cargo][0].left,Tree("=>",2,rules[s.cargo][0].right,wp_a(rules[s.cargo][1],p)))
	#if s.t == 8: #subst form
    #if s.t == 9: #role subst  

def app_s(s):
	#if s.t == 0: #binary prop & concept subst
    #if s.t == 1: #unary prop
	if s.t == 2: #binary strat: either concat or choice
		if s.cargo == ";": #concat
			return app_s(s.left)
		elif s.cargo == "+": #choice
			return Tree("|",2,app_s(s.left),app_s(s.right))
	elif s.t == 3: #parenthesized strat
		return app_s(s.cargo)
	#if s.t == 4: #quantifier
    #if s.t == 5: #negation form
	elif s.t == 6: #closure
		return Tree("^",7)
	elif s.t == 7: #simple rule application
		return rules[s.cargo][0]
	#if s.t == 8: #subst form
    #if s.t == 9: #role subst  

def vc(s,p):
	#if s.t == 0: #binary prop & concept subst
	#if s.t == 1: #unary prop
	if s.t == 2: #binary strat: either concat or choice	    
		if s.cargo == ";": #concat
			return Tree("&",2,vc(s.left,wp_s(s.right,p)),vc(s.right,p)) 
		elif s.cargo == "+": #choice
			return Tree("&",2,vc(s.left,p),vc(s.right,p))
	elif s.t == 3:
		return vc(s.cargo,p)
	#if s.t == 4: #quantifier
    #if s.t == 5: #negation form
	elif s.t == 6: #closure
		return Tree("&",2,Tree("&",2,Tree("=>",2,Tree("&",2,app_s(s.cargo),s.left),wp_s(s.cargo,s.left)),Tree("=>",2,Tree("&",2,Tree(app_s(s.cargo),5),s.left),p)),vc(s.cargo,s.left))
	elif s.t == 7: #simple rule application
		return Tree("^",7)	
	#if s.t == 8: #subst form
    #if s.t == 9: #role subst  

def supp_sub(t):
	if t.t == 0: #binary predicate
		return t
	elif t.t == 1: #unary predicate
		return t
	elif t.t == 2: #binop
		return Tree(t.cargo,2,supp_sub(t.left),supp_sub(t.right))
	elif t.t == 3: #parenthesis
		return Tree(supp_sub(t.cargo),3)
	elif t.t == 4: #quantifiers
		return Tree(t.cargo,4,t.left,supp_sub(t.right))
	elif t.t == 5: #negation
		return Tree(supp_sub(t.cargo),5)
	#elif t.t == 6: closure strat
	elif t.t == 7: #name strat & ^
		if t.cargo == "^":
			return t
	elif t.t == 8: #subst
		return app_subst(t.left,t.cargo)
	elif t.t == 10: #val compare
		return t
		
def app_subst(t,a):
	if t.t == 0: #binary predicate
		if a.t == 9: #action on a binary predicate
			if a.left == t.cargo: #same predicate
				if a.cargo == "add": #addition
					# R(a,b)[add(R,x,y)] -> R(a,b) u (x=a n y=b)  
					return Tree("|",2,t,Tree(Tree("&",2,Tree("=",0,t.left,a.right),Tree("=",0,t.right,a.center)),3))
				elif a.cargo == "del": #deletion
					# R(a,b)[del(R(x,y)] -> R(a,b) n (~(x=a) u ~(y=b))
					return Tree("&",2,t,Tree(Tree("|",2,Tree(Tree("=",0,t.left,a.right),5),Tree(Tree("=",0,t.right,a.center),5)),3))
			else:
				return t
		else:
			return t 
	elif t.t == 1: #unary predicate
		if a.t == 0: #action on an unary predicate
			if a.left == t.cargo: #same predicate
				if a.cargo == "add": #addition
					# P(a)[add(P,x)] -> P(a) u x=a
					return Tree("|",2,t,Tree("=",0,t.left,a.right))
				elif a.cargo == "del": #deletion
					# P(a)[del(p(x)] -> P(a) n ~(x = a)
					return Tree("&",2,t,Tree(Tree("=",0,t.left,a.right),5))
			else:
				return t
		else:
			return t 
	elif t.t == 2: #binop
		return Tree(t.cargo,2,app_subst(t.left,a),app_subst(t.right,a))
	elif t.t == 3: #parenthesis
		return Tree(app_subst(t.cargo,a),3)
	elif t.t == 4: #quantifiers
		return Tree(t.cargo,4,t.left,app_subst(t.right,a))
	elif t.t == 5: #negation
		return Tree(app_subst(t.cargo,a),5)
	#elif t.t == 6: closure strat
	elif t.t == 7: #name strat & ^
		if t.cargo == "^":
			return t
	elif t.t == 8: #subst
		return app_subst(app_subst(t.left,t.cargo),a)
	elif t.t == 10: #value compare
		return t

def wp_a(la,p):
	t = p
	for a in reversed(la): 
		t = Tree(a,8,t)
	return t

def ToZ3(f):
	if f.t == 0: #binary prop & concept subst
		if f.cargo == "=":
			return var[f.left] == var[f.right]
		else:
			return bpred[f.cargo](var[f.left],var[f.right])
	elif f.t == 1: #unary prop
		return upred[f.cargo](var[f.left])
	if f.t == 2: #binary op: either OR, AND, IMP or EQU	    
		if f.cargo == "|": #OR
			return Or(ToZ3(f.left),ToZ3(f.right)) 
		elif f.cargo == "&": #AND
			return And(ToZ3(f.left),ToZ3(f.right))
		elif f.cargo == "=>": #IMP
			return Implies(ToZ3(f.left),ToZ3(f.right))
		elif f.cargo == "<=>": #EQU
			return And(Implies(ToZ3(f.left),ToZ3(f.right)),Implies(ToZ3(f.right),ToZ3(f.left)))
	elif f.t == 3: #parenthesis
		return ToZ3(f.cargo)
	elif f.t == 4: #quantifier
		if f.cargo == "@":
			#print(ToZ3(f.right))
			#print(Exists([var['a']],ToZ3(f.right)))
			return Exists(map(ToVar,f.left),ToZ3(f.right))
		elif f.cargo == "#":
			return ForAll(map(ToVar,f.left),ToZ3(f.right))
	elif f.t == 5: #negation form
		return Not(ToZ3(f.cargo))
	#elif s.t == 6: #closure
	#	return Tree("&",2,Tree("&",2,Tree("=>",2,Tree("&",2,app_s(s.cargo),s.left),wp_s(s.cargo,s.left)),Tree("=>",2,Tree("&",2,Tree(app_s(s.cargo),5),s.left),p)),vc(s.cargo,s.left))
	elif f.t == 7: #True
		return BoolVal(True)	
	#if s.t == 8: #subst form
    #if s.t == 9: #role subst  
	elif f.t == 10: #value compare
		if f.left == "<":
			return ToVal(f.cargo) < ToVal(f.right)
		elif f.left == ">":
			return ToVal(f.cargo) > ToVal(f.right)
		elif f.left == "=":
			return ToVal(f.cargo) == ToVal(f.right)

def ToVal(f):
	if f.t == 11:
		return val(var[f.cargo])
	elif f.t == 12:
		return f.cargo

def ToVar(v):
	return var[v]

def p_error(p):
    if p:
        print("Syntax error at '%s'" % p.value)
    else:
        print("Syntax error at EOF")

#Main program

import ply.yacc as yacc
yacc.yacc()

Node = DeclareSort('Node')
val = Function('val',Node,IntSort())

while 1:
	try:
		s = raw_input()
		f = open(s,'r')
		yacc.parse(f.read())
	except EOFError:
		break
	if not s: continue
