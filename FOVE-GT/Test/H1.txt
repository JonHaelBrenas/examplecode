Pre:^
R0:{a;Pa(a)&~Pe(a)&~Pr(a)}->add(Pe,a),R1:{a;Pa(a)&~Pe(a)&~Pr(a)}->add(Pr,a)
(R0+R1)*{^}
Post:#x.(Pa(x)=>(Pe(x)|Pr(x)))
