package ca.unbsj.cbakerlab.sadi.services;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;

import ca.wilkinsonlab.sadi.service.annotations.Name;
import ca.wilkinsonlab.sadi.service.annotations.Description;
import ca.wilkinsonlab.sadi.service.annotations.ContactEmail;
import ca.wilkinsonlab.sadi.service.annotations.InputClass;
import ca.wilkinsonlab.sadi.service.annotations.OutputClass;
import ca.wilkinsonlab.sadi.service.simple.SimpleSynchronousServiceServlet;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedWriter;
import java.io.FileWriter;

import java .net.URL ;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java .util.Vector ;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

//import com.hp.hpl.jena.rdf.model.Statement;
//import com.hp.hpl.jena.rdf.model.StmtIterator;

@Name("getNameByPublicHealthActivityId")
@Description("Retrieves the name of a population health activity.")
@ContactEmail("sadnanalmanir@gmail.com")
@InputClass("http://cbakerlab.unbsj.ca:8080/siema-ontologies/service-ontologies/getNameByPublicHealthActivityId.owl#Input")
@OutputClass("http://cbakerlab.unbsj.ca:8080/siema-ontologies/service-ontologies/getNameByPublicHealthActivityId.owl#Output")
public class GetNameByPublicHealthActivityId extends SimpleSynchronousServiceServlet
{
	private static final Logger log = Logger.getLogger(GetNameByPublicHealthActivityId.class);
	private static final long serialVersionUID = 1L;
    
	@Override
	public void processInput(Resource input, Resource output)
	{
      
	    String interv_name = getData("interv.csv",input.getURI());
	    Model outputRDF = output.getModel();
	
	    output.addLiteral(Vocab.has_name, interv_name);

	}

	@SuppressWarnings("unused")
	private static final class Vocab
	{
		private static Model m_model = ModelFactory.createDefaultModel();
		
		public static final Property type = m_model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
	    public static final Property has_name = m_model.createProperty("http://cbakerlab.unbsj.ca:8080/siema-ontologies/domain-ontologies/malaria-intervention.owl#has_name");
		public static final Resource Input = m_model.createResource("http://cbakerlab.unbsj.ca:8080/siema-ontologies/service-ontologies/getNameByPublicHealthActivityId.owl#Input");
		public static final Resource Output = m_model.createResource("http://cbakerlab.unbsj.ca:8080/siema-ontologies/service-ontologies/getNameByPublicHealthActivityId.owl#Output");
	}

    private final String getData(String dbFile, String input) {
        
        try {
            InputStream stream = new FileInputStream(new File(this.getClass().getResource( "/"  + dbFile).toURI()));
            CSVReader reader = new CSVReader(new InputStreamReader(stream), ',' );
                
	    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
	    URL resource = classLoader.getResource(dbFile);
            Iterator<String[]> lines = reader.readAll().iterator();
	    
            while (lines.hasNext()) {
                String[] line = lines.next ();                
                String uri = "http://cbakerlab.unbsj.ca:8080" + resource.getPath().concat("#".concat("intervention".concat("/".concat(line[ 0 ]))));
		String name = line[1];
                if (uri.equals(input))
		     {
			 return name;
		     }
	    }
	    return "Not found";
        } catch (Exception ex) {
            throw new RuntimeException("Cannot read CSV file " + dbFile + " : " + ex, ex);
        }
    }
}
