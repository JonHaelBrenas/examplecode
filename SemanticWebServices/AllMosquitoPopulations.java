package ca.unbsj.cbakerlab.sadi.services;

import org.apache.log4j.Logger;

import ca.wilkinsonlab.sadi.service.annotations.Name;
import ca.wilkinsonlab.sadi.service.annotations.Description;
import ca.wilkinsonlab.sadi.service.annotations.ContactEmail;
import ca.wilkinsonlab.sadi.service.annotations.InputClass;
import ca.wilkinsonlab.sadi.service.annotations.OutputClass;
import ca.wilkinsonlab.sadi.service.simple.SimpleSynchronousServiceServlet;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

import au .com.bytecode.opencsv.CSVReader ;
import java .io.File ;
import java .io.FileInputStream ; 
import java.io.IOException;
import java .io.InputStream ; 
import java .io.InputStreamReader ; 
import java .net.URL ;
import java .util.Iterator ; 
import java .util.Map ;
import java .util.Vector ;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import java.io.BufferedWriter;
import java.io.FileWriter;

@Name("allMosquitoPopulations")
@Description("  Retrieve   all   mosquito   population.")
@ContactEmail("sadnanalmanir@gmail.com")
@InputClass("http://cbakerlab.unbsj.ca:8080/siema-ontologies/service-ontologies/allMosquitoPopulations.owl#Input")
@OutputClass("http://cbakerlab.unbsj.ca:8080/siema-ontologies/service-ontologies/allMosquitoPopulations.owl#Output")
public class AllMosquitoPopulations extends SimpleSynchronousServiceServlet
{
    private static final Logger log = Logger.getLogger(AllMosquitoPopulations.class);
    private static final long serialVersionUID = 1L;

    private Vector<Population> PopData = getData();

    private Vector<Population> getData()
    {
	Vector<Population> data = loadMosquito_Populations("pop1.csv");
	data.addAll(loadMosquito_Populations("pop2.csv"));
	data.addAll(loadMosquito_Pop_json("pop.json"));
	data.addAll(loadMosquito_Populations("pop5.csv"));
	data.addAll(loadMosquito_Populations("pop6.csv"));
	return data;
    }
    
    
    @Override
    public void processInput(Resource input, Resource output)
    {

	//Vector<Population> Mosq_pop = loadMosquito_Populations("pop1.csv" ); 
	Model outputRDF = output.getModel();
	Iterator<Population> mp = PopData.iterator();

	while (mp.hasNext()) {
	    String id = mp.next().getPop();
	    Resource mosquito = outputRDF.createResource(id);
	    mosquito.addProperty(Vocab.type, output );
	    mosquito.addLiteral(Vocab.has_name, id);
	}
    }
	
    @SuppressWarnings("unused")
    private static final class Vocab
    {
	private static Model m_model = ModelFactory.createDefaultModel();
		
	public static final Property type = m_model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
	public static final Resource Output = m_model.createResource("http://cbakerlab.unbsj.ca:8080/siema-ontologies/service-ontologies/allMosquitoPopulations.owl#Output");
	public static final Resource Input = m_model.createResource("http://cbakerlab.unbsj.ca:8080/siema-ontologies/service-ontologies/allMosquitoPopulations.owl#Input");
	public static final Property has_name = m_model.createProperty("http://cbakerlab.unbsj.ca:8080/siema-ontologies/domain-ontologies/malaria-terminology.owl#has_name");
    }
	
    private final Vector<Population> loadMosquito_Populations(String dbFile) {
        Vector<Population>   result   =  new    Vector<Population>();
        
        try {
            InputStream stream = new FileInputStream(new File(this.getClass().getResource( "/"  + dbFile).toURI()));
            CSVReader reader = new CSVReader(new InputStreamReader(stream), ',' );

            Iterator<String[]> lines = reader.readAll().iterator();
            lines. next ();  //   skip   the   headers
            //  Headers:
            //   MRN      Name      Height,   m         Weight,   kg
            while (lines.hasNext()) {
                String[] line = lines.next ();
                
                ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
                URL resource = classLoader.getResource(dbFile);
		//File   file   =   new   File(resource.getPath());
                
                String id = "http://cbakerlab.unbsj.ca:8080" + resource.getPath().concat("#".concat("population".concat("/".concat(line[ 0 ]))));
                Population pop = new Population(id);
                result.add( pop );
            }    
            
        } catch (Exception ex) {
            throw new RuntimeException("Cannot read CSV file " + dbFile + " : " + ex, ex);
        }
        return result;
    }
    
    private final Vector<Population> loadMosquito_Pop_json(String dbFile) {
        Vector<Population> result = new Vector<Population>();

        
        try {
            InputStream stream = new FileInputStream(new File(this.getClass().getResource("/" + dbFile).toURI()));
            InputStreamReader isr = new InputStreamReader(stream);
            
            JsonReader reader = new JsonReader(isr);
						   
            String key = null;
            key = parseJSON(reader, result, key, dbFile);
            reader.close();

        } catch (Exception ex) {
            throw new RuntimeException("Cannot read file " + dbFile + " : " + ex, ex);
        }
        return result;
    }
    
    
    
    private static String parseJSON(JsonReader reader, Vector<Population> result, String key, String dbFile) throws IOException {
	//loop to read all tokens
	while(reader.hasNext()){
	    //get next token
	    JsonToken token = reader.peek();
	    switch(token){
	    case BEGIN_OBJECT:
		reader.beginObject();
		while(reader.hasNext()){
		    parseJSON(reader, result, key, dbFile);
		}
		reader.endObject();
		break;

	    case END_OBJECT:
		reader.endObject();
		break;
	    case BEGIN_ARRAY:
		reader.beginArray();
		while(reader.hasNext()){
		    parseJSON(reader, result, key, dbFile);
		}
		reader.endArray();
		break;
	    case END_ARRAY:
		reader.endArray();
		break;
	    case NAME:
		key = reader.nextName();
		break;
	    case STRING:
		if("population".equals(key))
		    {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			URL resource = classLoader.getResource(dbFile);
			//File   file   =   new   File(resource.getPath());
                
			String id = "http://cbakerlab.unbsj.ca:8080" + resource.getPath().concat("#".concat("population".concat("/".concat(reader.nextString()))));
			result.add(new Population(id));
		    } else
		    {
			reader.nextString();
		    }
		break;
	    case NULL:
		reader.nextNull();
		break;
	    case END_DOCUMENT:
		return key;
	    default:
		break;
	    }
	}
	return key;
    }
}
