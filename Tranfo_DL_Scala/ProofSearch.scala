package isa

import VerifCondition._

import com.microsoft.z3._

object ProofSearch {

  abstract sealed class proof_method
  final case class ALCQ() extends proof_method
  final case class Z3() extends proof_method

  def search_pt(trc: List[trace_constr[String, String, String]],
    brs: List[branch[String, String, String]]): proof_result[trace[String, String, String], branch[String, String, String]] =
    (trc, brs) match {
      case (List(CTrace(tr)), List()) => TablUnsatisf(tr)
      case (traces, br :: brs) =>
        if (contains_clash_branch(br))
          failed_subproof(Trace(Clash_rep(), List()), traces) match {
            case None => InternalError()
            case Some(tr) => search_pt(tr, brs)
          }
        else if (is_inactive_string(br))
          TablSatisf(br)
        else apply_rules_pt[String, String, String](br) match {
          case AppRes(_, None, new_brs) => search_pt(traces, new_brs ++ brs)
          case AppRes(n, Some(rr), new_brs) => search_pt(ITrace(List(), n, rr) :: traces, new_brs ++ brs)
        }
    }

  def prove_pt(fs: List[form[String, String, String]]) =
    search_pt(List(), List(Branch(fs, (List(), (List(), (List(),
      Inactive_form(List(), (List(), (List(), (List(), List()))))))))))

  def prove_validity(pm: proof_method, vcs: qform[String, String, String]): proof_result[trace[String, String, String], branch[String, String, String]] =
    {
      val neg_prenex_f = neg_norm_form(false, free_prenex_form_list_string(vcs));
      val init_branch = List(neg_prenex_f);
      /* TODO: remove following   */
      println("proving validity of:");
      println(vcs);
      println("... satisfiability of:");
      println(init_branch);
      // end remove following
      pm match {
        case ALCQ() => prove_pt(init_branch)
        case Z3() => prove_pt(init_branch)
      }
    }

  def PLFormB2SMTLIB(plf: plform[String, String, vara[String]]): String =
  {
     return PLFormB2SMTLIB(plf,List())
  }

  def PLFormB2SMTLIB(plf: plform[String, String, vara[String]], env: List[String]): String =
    {
      plf match {
        case PlConstFm(a) => a match {
          case true => return "true"
          case false => return "(not true)"
        }
        case PlConc(a, b) => b match{
          case Free(x) => return "(" + a + " " + x + ")"
          case Bound(x) => return "(" + a + " " + nth[String](env,x) + ")"
        }
        case PlRel(a, b, c, d) => (a,c,d) match {
          case (true,Free(x),Free(y)) => return "(" + b + " " + x + " " + y + ")"
          case (true,Bound(x),Free(y)) => return "(" + b + " " + nth[String](env,x) + " " + y + ")"
          case (true,Free(x),Bound(y)) => return "(" + b + " " + x + " " + nth[String](env,y) + ")"
          case (true,Bound(x),Bound(y)) => return "(" + b + " " + nth[String](env,x) + " " + nth[String](env,y) + ")"
          case (false,Free(x),Free(y)) => return "(not " + "(" + b + " " + x + " " + y + "))"
          case (false,Bound(x),Free(y)) => return "(not (" + b + " " + nth[String](env,x) + " " + y + "))"
          case (false,Free(x),Bound(y)) => return "(not (" + b + " " + x + " " + nth[String](env,y) + "))"
          case (false,Bound(x),Bound(y)) => return "(not (" + b + " " + nth[String](env,x) + " " + nth[String](env,y) + "))"
        }
        case PlEq(a, b, c) => (a,b,c) match {
          case (true,Free(x),Free(y)) => return "(= " + x + " " + y + ")"
          case (true,Bound(x),Free(y)) => return "(= " + nth[String](env,x) + " " + y + ")"
          case (true,Free(x),Bound(y)) => return "(= " + x + " " + nth[String](env,y) + ")"
          case (true,Bound(x),Bound(y)) => return "(= "  + nth[String](env,x) + " " + nth[String](env,y) + ")"
          case (false,Free(x),Free(y)) => return "(not (= "  + x + " " + y + "))"
          case (false,Bound(x),Free(y)) => return "(not (= " + nth[String](env,x) + " " + y + "))"
          case (false,Free(x),Bound(y)) => return "(not (= " + x + " " + nth[String](env,y) + "))"
          case (false,Bound(x),Bound(y)) => return "(not (= " + nth[String](env,x) + " " + nth[String](env,y) + "))"
        }
        case PlNegFm(a) => return "(not " + PLFormB2SMTLIB(a,env) + ")"
        case PlBinopFm(a, b, c) => a match {
          case Conj() => return "(and " + PLFormB2SMTLIB(b,env) + " " + PLFormB2SMTLIB(c,env) + ")"
          case Disj() => return "(or " + PLFormB2SMTLIB(b,env) + " " + PLFormB2SMTLIB(c,env) + ")"
        }
        case PlQuantifFm(a, b, c) => 
          var t = ""
          var e = b
          a match {
            case QEx() =>
              t = t + "(exists (("
            case QAll() =>
              t = t + "(exists (("
          }
          while (e != null){
            t = t + e.head + " Node)"
            e = e.tail
          }
          t = t + ")"
          return t + PLFormB2SMTLIB(c,b ++ env) + ")"  
      }
    }

    def SMTest()
    {
      var ctx = new com.microsoft.z3.Context()
      var S = ctx.mkSolver()
      var x = ctx.mkIntConst("x")
      var y = ctx.mkIntConst("y")
      var e = ctx.mkEq(x,y)
      var X = ctx.mkLt(x,y)
      S.add(e,X)
      if (S.check() == Status.SATISFIABLE)
        println("test")
      else
        println("not test")
    }
}