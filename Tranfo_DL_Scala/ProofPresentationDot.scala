package isa

// imported scala library files
import scala.sys.process._
// imported scala files
import VerifCondition._

object ProofPresentationDot {

  def nodes_and_arcs[R: equal, C: equal, I: equal](abox: List[form[R, C, I]]): (List[(I, List[(Boolean, C)])], List[(R, (I, I))]) =
    {
      val canon_i = canon_interp_impl(abox);
      val nodes = interp_impl_i(canon_i);
      val rels = interp_impl_r(canon_i);
      println("in nodes and arcs");
      println(nodes);
      println(rels);
      val node_infos = nodes map (i => (i, interp_impl_i_c(canon_i)(i)));
      val rel_infos = rels flatMap (r => (interp_impl_r_i(canon_i)(r)) map (p => (r, p)));
      (node_infos, rel_infos)
    }

  def print_list[A](xs: List[A], f: A => String, startstr: String, sepstr: String, endstr: String): String =
    (xs.foldLeft(startstr)((x, y) => x ++ f(y) ++ sepstr)) ++ endstr;

  def print_node_name(n: String) = n

  def print_arc(a: (String, (String, String))): String = {
    val reln = a._1;
    val n1 = a._2._1;
    val n2 = a._2._2;
    (print_node_name(n1)) ++ " -> " ++ (print_node_name(n2)) ++ " [label= \" " ++ reln ++ " \" ]"
  }

  def print_arcs(rels: List[(String, (String, String))]): String =
    print_list[(String, (String, String))](rels, (a => print_arc(a)), "", "\n", "");

  def print_concept_info(ci: (Boolean, String)): String =
    (if (ci._1) { "" } else "-") ++ ci._2

  def print_node(ni: (String, List[(Boolean, String)])): String = {
    val n_name = print_node_name(ni._1);
    val node_info = print_list[(Boolean, String)](ni._2,
      (ci => print_concept_info(ci)), (" [label= \"" ++ n_name ++ ": "), " ", " \" ] \n");
    n_name ++ node_info
  }

  def print_nodes(node_infos: List[(String, List[(Boolean, String)])]): String =
    print_list[(String, List[(Boolean, String)])](node_infos, (ni => print_node(ni)), "", "\n", "")

  def print_graph(grname: String, ginfo: (List[(String, List[(Boolean, String)])], List[(String, (String, String))])): String =
    "digraph " ++ grname ++ " {\n" ++
      print_nodes(ginfo._1) ++
      print_arcs(ginfo._2) ++ "}\n"

  def generate_dot_file(filename_prefix: String, md: List[form[String, String, String]]) {
    val dot_filename = filename_prefix ++ ".gv";
    val pdf_filename = filename_prefix ++ ".pdf";
    val dot_command = "dot " ++ dot_filename ++ " -Tpdf -o " ++ pdf_filename;
    val graph_string = print_graph(filename_prefix, nodes_and_arcs(md));
    val dot_file = new java.io.FileWriter(dot_filename);
    dot_file.write(graph_string);
    dot_file.close;
    "pwd".!;
    dot_command.!;
  }

  def analyze_result(filename_prefix: String, res: proof_result[trace[String, String, String], branch[String, String, String]]): Boolean =
    res match {
      case TablUnsatisf(abox) =>
        System.out.println("tableau not satisfiable\n");
        //println("proof result with trace:")
        //println(abox);
        generate_dot_file(filename_prefix, List());
        true
      case TablSatisf(abox) =>
        System.out.println("tableau satisfiable\n");
        /* TODO: remove following    */
        val allfms = all_forms(abox);
        val naa = nodes_and_arcs(allfms);
        //println("proof result with trace:")
        //println(abox);
        println("proof result - forms:")
        println(allfms);
        println("proof result - nodes and arcs:");
        println(naa);
        // end remove following

        generate_dot_file(filename_prefix, all_forms(abox));
        false
      case InternalError() =>
        System.out.println("internal error\n");
        generate_dot_file(filename_prefix, List());
        false
    }

}
