package isa

object VerifCondition {

  trait equal[A] {
    val `VerifCondition.equal`: (A, A) => Boolean
  }
  def equal[A](a: A, b: A)(implicit A: equal[A]): Boolean =
    A.`VerifCondition.equal`(a, b)

  def eq[A: equal](a: A, b: A): Boolean = equal[A](a, b)

  def equal_lista[A: equal](x0: List[A], x1: List[A]): Boolean = (x0, x1) match {
    case (Nil, x21 :: x22) => false
    case (x21 :: x22, Nil) => false
    case (x21 :: x22, y21 :: y22) => eq[A](x21, y21) && equal_lista[A](x22, y22)
    case (Nil, Nil) => true
  }

  implicit def equal_list[A: equal]: equal[List[A]] = new equal[List[A]] {
    val `VerifCondition.equal` = (a: List[A], b: List[A]) => equal_lista[A](a, b)
  }

  abstract sealed class nat
  final case class Nat(a: BigInt) extends nat

  def integer_of_nat(x0: nat): BigInt = x0 match {
    case Nat(x) => x
  }

  def equal_nat(m: nat, n: nat): Boolean = integer_of_nat(m) == integer_of_nat(n)

  abstract sealed class vara[A]
  final case class Free[A](a: A) extends vara[A]
  final case class Bound[A](a: nat) extends vara[A]

  def equal_vara[A: equal](x0: vara[A], x1: vara[A]): Boolean = (x0, x1) match {
    case (Free(x1), Bound(x2)) => false
    case (Bound(x2), Free(x1)) => false
    case (Bound(x2), Bound(y2)) => equal_nat(x2, y2)
    case (Free(x1), Free(y1)) => eq[A](x1, y1)
  }

  implicit def equal_var[A: equal]: equal[vara[A]] = new equal[vara[A]] {
    val `VerifCondition.equal` = (a: vara[A], b: vara[A]) => equal_vara[A](a, b)
  }

  implicit def equal_char: equal[Char] = new equal[Char] {
    val `VerifCondition.equal` = (a: Char, b: Char) => a == b
  }

  def equal_bool(p: Boolean, pa: Boolean): Boolean = (p, pa) match {
    case (p, true) => p
    case (p, false) => !p
    case (true, p) => p
    case (false, p) => !p
  }

  abstract sealed class subst_op
  final case class SDiff() extends subst_op
  final case class SAdd() extends subst_op

  def equal_subst_op(x0: subst_op, x1: subst_op): Boolean = (x0, x1) match {
    case (SDiff(), SAdd()) => false
    case (SAdd(), SDiff()) => false
    case (SAdd(), SAdd()) => true
    case (SDiff(), SDiff()) => true
  }

  def equal_prod[A: equal, B: equal](x0: (A, B), x1: (A, B)): Boolean = (x0, x1) match {
    case ((x1, x2), (y1, y2)) => eq[A](x1, y1) && eq[B](x2, y2)
  }

  abstract sealed class subst[A, B, C]
  final case class RSubst[A, C, B](a: A, b: subst_op, c: (C, C)) extends subst[A, B, C]
  final case class CSubst[B, C, A](a: B, b: subst_op, c: C) extends subst[A, B, C]

  def equal_subst[A: equal, B: equal, C: equal](x0: subst[A, B, C], x1: subst[A, B, C]): Boolean =
    (x0, x1) match {
      case (RSubst(x11, x12, x13), CSubst(x21, x22, x23)) => false
      case (CSubst(x21, x22, x23), RSubst(x11, x12, x13)) => false
      case (CSubst(x21, x22, x23), CSubst(y21, y22, y23)) =>
        eq[B](x21, y21) && (equal_subst_op(x22, y22) && eq[C](x23, y23))
      case (RSubst(x11, x12, x13), RSubst(y11, y12, y13)) =>
        eq[A](x11, y11) && (equal_subst_op(x12, y12) && equal_prod[C, C](x13, y13))
    }

  abstract sealed class binop
  final case class Conj() extends binop
  final case class Disj() extends binop

  def equal_binop(x0: binop, x1: binop): Boolean = (x0, x1) match {
    case (Conj(), Disj()) => false
    case (Disj(), Conj()) => false
    case (Disj(), Disj()) => true
    case (Conj(), Conj()) => true
  }

  abstract sealed class numres_ord
  final case class Lt() extends numres_ord
  final case class Ge() extends numres_ord

  def equal_numres_ord(x0: numres_ord, x1: numres_ord): Boolean = (x0, x1) match {
    case (Lt(), Ge()) => false
    case (Ge(), Lt()) => false
    case (Ge(), Ge()) => true
    case (Lt(), Lt()) => true
  }

  abstract sealed class concept[A, B, C]
  final case class Top[A, B, C]() extends concept[A, B, C]
  final case class Bottom[A, B, C]() extends concept[A, B, C]
  final case class AtomC[B, A, C](a: Boolean, b: B) extends concept[A, B, C]
  final case class NegC[A, B, C](a: concept[A, B, C]) extends concept[A, B, C]
  final case class BinopC[A, B, C](a: binop, b: concept[A, B, C], c: concept[A, B, C])
    extends concept[A, B, C]
  final case class NumRestrC[A, B, C](a: numres_ord, b: nat, c: A, d: concept[A, B, C])
    extends concept[A, B, C]
  final case class SubstC[A, B, C](a: concept[A, B, C], b: subst[A, B, C]) extends concept[A, B, C]
  final case class SomeC[A, B, C](a: A, b: concept[A, B, C]) extends concept[A, B, C]
  final case class AllC[A, B, C](a: A, b: concept[A, B, C]) extends concept[A, B, C]

  def equal_concept[A: equal, B: equal, C: equal](x0: concept[A, B, C], x1: concept[A, B, C]): Boolean =
    (x0, x1) match {
      case (SomeC(x81, x82), AllC(x91, x92)) => false
      case (AllC(x91, x92), SomeC(x81, x82)) => false
      case (SubstC(x71, x72), AllC(x91, x92)) => false
      case (AllC(x91, x92), SubstC(x71, x72)) => false
      case (SubstC(x71, x72), SomeC(x81, x82)) => false
      case (SomeC(x81, x82), SubstC(x71, x72)) => false
      case (NumRestrC(x61, x62, x63, x64), AllC(x91, x92)) => false
      case (AllC(x91, x92), NumRestrC(x61, x62, x63, x64)) => false
      case (NumRestrC(x61, x62, x63, x64), SomeC(x81, x82)) => false
      case (SomeC(x81, x82), NumRestrC(x61, x62, x63, x64)) => false
      case (NumRestrC(x61, x62, x63, x64), SubstC(x71, x72)) => false
      case (SubstC(x71, x72), NumRestrC(x61, x62, x63, x64)) => false
      case (BinopC(x51, x52, x53), AllC(x91, x92)) => false
      case (AllC(x91, x92), BinopC(x51, x52, x53)) => false
      case (BinopC(x51, x52, x53), SomeC(x81, x82)) => false
      case (SomeC(x81, x82), BinopC(x51, x52, x53)) => false
      case (BinopC(x51, x52, x53), SubstC(x71, x72)) => false
      case (SubstC(x71, x72), BinopC(x51, x52, x53)) => false
      case (BinopC(x51, x52, x53), NumRestrC(x61, x62, x63, x64)) => false
      case (NumRestrC(x61, x62, x63, x64), BinopC(x51, x52, x53)) => false
      case (NegC(x4), AllC(x91, x92)) => false
      case (AllC(x91, x92), NegC(x4)) => false
      case (NegC(x4), SomeC(x81, x82)) => false
      case (SomeC(x81, x82), NegC(x4)) => false
      case (NegC(x4), SubstC(x71, x72)) => false
      case (SubstC(x71, x72), NegC(x4)) => false
      case (NegC(x4), NumRestrC(x61, x62, x63, x64)) => false
      case (NumRestrC(x61, x62, x63, x64), NegC(x4)) => false
      case (NegC(x4), BinopC(x51, x52, x53)) => false
      case (BinopC(x51, x52, x53), NegC(x4)) => false
      case (AtomC(x31, x32), AllC(x91, x92)) => false
      case (AllC(x91, x92), AtomC(x31, x32)) => false
      case (AtomC(x31, x32), SomeC(x81, x82)) => false
      case (SomeC(x81, x82), AtomC(x31, x32)) => false
      case (AtomC(x31, x32), SubstC(x71, x72)) => false
      case (SubstC(x71, x72), AtomC(x31, x32)) => false
      case (AtomC(x31, x32), NumRestrC(x61, x62, x63, x64)) => false
      case (NumRestrC(x61, x62, x63, x64), AtomC(x31, x32)) => false
      case (AtomC(x31, x32), BinopC(x51, x52, x53)) => false
      case (BinopC(x51, x52, x53), AtomC(x31, x32)) => false
      case (AtomC(x31, x32), NegC(x4)) => false
      case (NegC(x4), AtomC(x31, x32)) => false
      case (Bottom(), AllC(x91, x92)) => false
      case (AllC(x91, x92), Bottom()) => false
      case (Bottom(), SomeC(x81, x82)) => false
      case (SomeC(x81, x82), Bottom()) => false
      case (Bottom(), SubstC(x71, x72)) => false
      case (SubstC(x71, x72), Bottom()) => false
      case (Bottom(), NumRestrC(x61, x62, x63, x64)) => false
      case (NumRestrC(x61, x62, x63, x64), Bottom()) => false
      case (Bottom(), BinopC(x51, x52, x53)) => false
      case (BinopC(x51, x52, x53), Bottom()) => false
      case (Bottom(), NegC(x4)) => false
      case (NegC(x4), Bottom()) => false
      case (Bottom(), AtomC(x31, x32)) => false
      case (AtomC(x31, x32), Bottom()) => false
      case (Top(), AllC(x91, x92)) => false
      case (AllC(x91, x92), Top()) => false
      case (Top(), SomeC(x81, x82)) => false
      case (SomeC(x81, x82), Top()) => false
      case (Top(), SubstC(x71, x72)) => false
      case (SubstC(x71, x72), Top()) => false
      case (Top(), NumRestrC(x61, x62, x63, x64)) => false
      case (NumRestrC(x61, x62, x63, x64), Top()) => false
      case (Top(), BinopC(x51, x52, x53)) => false
      case (BinopC(x51, x52, x53), Top()) => false
      case (Top(), NegC(x4)) => false
      case (NegC(x4), Top()) => false
      case (Top(), AtomC(x31, x32)) => false
      case (AtomC(x31, x32), Top()) => false
      case (Top(), Bottom()) => false
      case (Bottom(), Top()) => false
      case (AllC(x91, x92), AllC(y91, y92)) =>
        eq[A](x91, y91) && equal_concept[A, B, C](x92, y92)
      case (SomeC(x81, x82), SomeC(y81, y82)) =>
        eq[A](x81, y81) && equal_concept[A, B, C](x82, y82)
      case (SubstC(x71, x72), SubstC(y71, y72)) =>
        equal_concept[A, B, C](x71, y71) && equal_subst[A, B, C](x72, y72)
      case (NumRestrC(x61, x62, x63, x64), NumRestrC(y61, y62, y63, y64)) =>
        equal_numres_ord(x61, y61) &&
          (equal_nat(x62, y62) &&
            (eq[A](x63, y63) && equal_concept[A, B, C](x64, y64)))
      case (BinopC(x51, x52, x53), BinopC(y51, y52, y53)) =>
        equal_binop(x51, y51) &&
          (equal_concept[A, B, C](x52, y52) && equal_concept[A, B, C](x53, y53))
      case (NegC(x4), NegC(y4)) => equal_concept[A, B, C](x4, y4)
      case (AtomC(x31, x32), AtomC(y31, y32)) =>
        equal_bool(x31, y31) && eq[B](x32, y32)
      case (Bottom(), Bottom()) => true
      case (Top(), Top()) => true
    }

  abstract sealed class fact[A, B, C]
  final case class Inst[C, A, B](a: C, b: concept[A, B, C]) extends fact[A, B, C]
  final case class AtomR[A, C, B](a: Boolean, b: A, c: C, d: C) extends fact[A, B, C]
  final case class Eq[C, A, B](a: Boolean, b: C, c: C) extends fact[A, B, C]

  def equal_fact[A: equal, B: equal, C: equal](x0: fact[A, B, C], x1: fact[A, B, C]): Boolean =
    (x0, x1) match {
      case (AtomR(x21, x22, x23, x24), Eq(x31, x32, x33)) => false
      case (Eq(x31, x32, x33), AtomR(x21, x22, x23, x24)) => false
      case (Inst(x11, x12), Eq(x31, x32, x33)) => false
      case (Eq(x31, x32, x33), Inst(x11, x12)) => false
      case (Inst(x11, x12), AtomR(x21, x22, x23, x24)) => false
      case (AtomR(x21, x22, x23, x24), Inst(x11, x12)) => false
      case (Eq(x31, x32, x33), Eq(y31, y32, y33)) =>
        equal_bool(x31, y31) && (eq[C](x32, y32) && eq[C](x33, y33))
      case (AtomR(x21, x22, x23, x24), AtomR(y21, y22, y23, y24)) =>
        equal_bool(x21, y21) &&
          (eq[A](x22, y22) && (eq[C](x23, y23) && eq[C](x24, y24)))
      case (Inst(x11, x12), Inst(y11, y12)) =>
        eq[C](x11, y11) && equal_concept[A, B, C](x12, y12)
    }

  abstract sealed class form[A, B, C]
  final case class ConstFm[A, B, C](a: Boolean) extends form[A, B, C]
  final case class FactFm[A, B, C](a: fact[A, B, C]) extends form[A, B, C]
  final case class NegFm[A, B, C](a: form[A, B, C]) extends form[A, B, C]
  final case class BinopFm[A, B, C](a: binop, b: form[A, B, C], c: form[A, B, C])
    extends form[A, B, C]
  final case class SubstFm[A, B, C](a: form[A, B, C], b: subst[A, B, C]) extends form[A, B, C]

  def equal_forma[A: equal, B: equal, C: equal](x0: form[A, B, C], x1: form[A, B, C]): Boolean =
    (x0, x1) match {
      case (BinopFm(x41, x42, x43), SubstFm(x51, x52)) => false
      case (SubstFm(x51, x52), BinopFm(x41, x42, x43)) => false
      case (NegFm(x3), SubstFm(x51, x52)) => false
      case (SubstFm(x51, x52), NegFm(x3)) => false
      case (NegFm(x3), BinopFm(x41, x42, x43)) => false
      case (BinopFm(x41, x42, x43), NegFm(x3)) => false
      case (FactFm(x2), SubstFm(x51, x52)) => false
      case (SubstFm(x51, x52), FactFm(x2)) => false
      case (FactFm(x2), BinopFm(x41, x42, x43)) => false
      case (BinopFm(x41, x42, x43), FactFm(x2)) => false
      case (FactFm(x2), NegFm(x3)) => false
      case (NegFm(x3), FactFm(x2)) => false
      case (ConstFm(x1), SubstFm(x51, x52)) => false
      case (SubstFm(x51, x52), ConstFm(x1)) => false
      case (ConstFm(x1), BinopFm(x41, x42, x43)) => false
      case (BinopFm(x41, x42, x43), ConstFm(x1)) => false
      case (ConstFm(x1), NegFm(x3)) => false
      case (NegFm(x3), ConstFm(x1)) => false
      case (ConstFm(x1), FactFm(x2)) => false
      case (FactFm(x2), ConstFm(x1)) => false
      case (SubstFm(x51, x52), SubstFm(y51, y52)) =>
        equal_forma[A, B, C](x51, y51) && equal_subst[A, B, C](x52, y52)
      case (BinopFm(x41, x42, x43), BinopFm(y41, y42, y43)) =>
        equal_binop(x41, y41) &&
          (equal_forma[A, B, C](x42, y42) && equal_forma[A, B, C](x43, y43))
      case (NegFm(x3), NegFm(y3)) => equal_forma[A, B, C](x3, y3)
      case (FactFm(x2), FactFm(y2)) => equal_fact[A, B, C](x2, y2)
      case (ConstFm(x1), ConstFm(y1)) => equal_bool(x1, y1)
    }

  implicit def equal_form[A: equal, B: equal, C: equal]: equal[form[A, B, C]] =
    new equal[form[A, B, C]] {
      val `VerifCondition.equal` = (a: form[A, B, C], b: form[A, B, C]) =>
        equal_forma[A, B, C](a, b)
    }

  implicit def equal_literal: equal[String] = new equal[String] {
    val `VerifCondition.equal` = (a: String, b: String) => a == b
  }

  trait ord[A] {
    val `VerifCondition.less_eq`: (A, A) => Boolean
    val `VerifCondition.less`: (A, A) => Boolean
  }
  def less_eq[A](a: A, b: A)(implicit A: ord[A]): Boolean =
    A.`VerifCondition.less_eq`(a, b)
  def less[A](a: A, b: A)(implicit A: ord[A]): Boolean =
    A.`VerifCondition.less`(a, b)

  implicit def ord_literal: ord[String] = new ord[String] {
    val `VerifCondition.less_eq` = (a: String, b: String) => a <= b
    val `VerifCondition.less` = (a: String, b: String) => a < b
  }

  def map[A, B](f: A => B, x1: List[A]): List[B] = (f, x1) match {
    case (f, Nil) => Nil
    case (f, x21 :: x22) => f(x21) :: map[A, B](f, x22)
  }

  def remove1[A: equal](x: A, xa1: List[A]): List[A] = (x, xa1) match {
    case (x, Nil) => Nil
    case (x, y :: xs) => (if (eq[A](x, y)) xs else y :: remove1[A](x, xs))
  }

  def less_eq_nat(m: nat, n: nat): Boolean =
    integer_of_nat(m) <= integer_of_nat(n)

  def max[A: ord](a: A, b: A): A = (if (less_eq[A](a, b)) b else a)

  implicit def ord_integer: ord[BigInt] = new ord[BigInt] {
    val `VerifCondition.less_eq` = (a: BigInt, b: BigInt) => a <= b
    val `VerifCondition.less` = (a: BigInt, b: BigInt) => a < b
  }

  def nat_of_integer(k: BigInt): nat = Nat(max[BigInt](BigInt(0), k))

  def plus_nat(m: nat, n: nat): nat = Nat(integer_of_nat(m) + integer_of_nat(n))

  def less_nat(m: nat, n: nat): Boolean = integer_of_nat(m) < integer_of_nat(n)

  abstract sealed class num
  final case class One() extends num
  final case class Bit0(a: num) extends num
  final case class Bit1(a: num) extends num

  def one_nat: nat = Nat(BigInt(1))

  abstract sealed class nibble
  final case class Nibble0() extends nibble
  final case class Nibble1() extends nibble
  final case class Nibble2() extends nibble
  final case class Nibble3() extends nibble
  final case class Nibble4() extends nibble
  final case class Nibble5() extends nibble
  final case class Nibble6() extends nibble
  final case class Nibble7() extends nibble
  final case class Nibble8() extends nibble
  final case class Nibble9() extends nibble
  final case class NibbleA() extends nibble
  final case class NibbleB() extends nibble
  final case class NibbleC() extends nibble
  final case class NibbleD() extends nibble
  final case class NibbleE() extends nibble
  final case class NibbleF() extends nibble

  def comp[A, B, C](f: A => B, g: C => A): C => B = ((x: C) => f(g(x)))

  def nat_of_char: Char => nat =
    comp[BigInt, nat, Char](((a: BigInt) => nat_of_integer(a)),
      ((a: Char) => BigInt(a.toInt)))

  def char_of_nat: nat => Char =
    comp[BigInt, Char, nat](((k: BigInt) => if (BigInt(0) <= k && k < BigInt(256)) k.charValue else error("character value out of range")),
      ((a: nat) => integer_of_nat(a)))

  def nulla[A](x0: List[A]): Boolean = x0 match {
    case Nil => true
    case x :: xs => false
  }

  def butlast[A](x0: List[A]): List[A] = x0 match {
    case Nil => Nil
    case x :: xs => (if (nulla[A](xs)) Nil else x :: butlast[A](xs))
  }

  def last[A](x0: List[A]): A = x0 match {
    case x :: xs => (if (nulla[A](xs)) x else last[A](xs))
  }

  def upChar(y: List[Char]): List[Char] =
    (if (!(nulla[Char](y)) &&
      (less_eq_nat(nat_of_integer(BigInt(97)),
        nat_of_char.apply(last[Char](y))) &&
        less_nat(nat_of_char.apply(last[Char](y)),
          nat_of_integer(BigInt(122)))))
      butlast[Char](y) ++
      List(char_of_nat.apply(plus_nat(nat_of_char.apply(last[Char](y)),
        one_nat)))
    else y ++ List('a'))

  def member[A: equal](x0: List[A], y: A): Boolean = (x0, y) match {
    case (Nil, y) => false
    case (x :: xs, y) => eq[A](x, y) || member[A](xs, y)
  }

  def fresh(xs: List[List[Char]], y: List[Char]): List[Char] =
    (if (member[List[Char]](xs, y)) fresh(remove1[List[Char]](y, xs), upChar(y))
    else y)

  def fresh_string_literal(strs: List[String], str: String): String =
    ("" ++
      (fresh(map[String, List[Char]](((a: String) => (a.toList)), strs),
        (str.toList))))

  def new_var_list_literal: (List[String]) => String => String =
    ((a: List[String]) => (b: String) => fresh_string_literal(a, b))

  trait new_var_list_class[A] {
    val `VerifCondition.new_var_list`: (List[A], A) => A
  }
  def new_var_list[A](a: List[A], b: A)(implicit A: new_var_list_class[A]): A =
    A.`VerifCondition.new_var_list`(a, b)

  implicit def new_var_list_class_literal: new_var_list_class[String] = new new_var_list_class[String] {
    val `VerifCondition.new_var_list` = (a: List[String], b: String) =>
      new_var_list_literal.apply(a).apply(b)
  }

  abstract sealed class quantif
  final case class QAll() extends quantif
  final case class QEx() extends quantif

  abstract sealed class rename[A]
  final case class ISubst[A](a: A, b: A) extends rename[A]

  abstract sealed class qform[A, B, C]
  final case class QConstFm[A, B, C](a: Boolean) extends qform[A, B, C]
  final case class QFactFm[A, B, C](a: fact[A, B, vara[C]]) extends qform[A, B, C]
  final case class QNegFm[A, B, C](a: qform[A, B, C]) extends qform[A, B, C]
  final case class QBinopFm[A, B, C](a: binop, b: qform[A, B, C], c: qform[A, B, C])
    extends qform[A, B, C]
  final case class QuantifFm[C, A, B](a: quantif, b: C, c: qform[A, B, C]) extends qform[A, B, C]
  final case class QSubstFm[A, B, C](a: qform[A, B, C], b: subst[A, B, vara[C]])
    extends qform[A, B, C]
  final case class QRenameFm[A, B, C](a: qform[A, B, C], b: rename[vara[C]])
    extends qform[A, B, C]

  abstract sealed class stmt[A, B, C]
  final case class Skip[A, B, C]() extends stmt[A, B, C]
  final case class NAdd[C, B, A](a: C, b: B) extends stmt[A, B, C]
  final case class NDel[C, B, A](a: C, b: B) extends stmt[A, B, C]
  final case class EAdd[C, A, B](a: C, b: A, c: C) extends stmt[A, B, C]
  final case class EDel[C, A, B](a: C, b: A, c: C) extends stmt[A, B, C]
  final case class SelAss[C, A, B](a: List[C], b: form[A, B, C]) extends stmt[A, B, C]
  final case class Seq[A, B, C](a: stmt[A, B, C], b: stmt[A, B, C]) extends stmt[A, B, C]
  final case class If[A, B, C](a: form[A, B, C], b: stmt[A, B, C], c: stmt[A, B, C])
    extends stmt[A, B, C]
  final case class While[A, B, C](a: form[A, B, C], b: form[A, B, C], c: stmt[A, B, C])
    extends stmt[A, B, C]

  abstract sealed class plform[A, B, C]
  final case class PlConstFm[A, B, C](a: Boolean) extends plform[A, B, C]
  final case class PlConc[B, C, A](a: B, b: C) extends plform[A, B, C]
  final case class PlRel[A, C, B](a: Boolean, b: A, c: C, d: C) extends plform[A, B, C]
  final case class PlEq[C, A, B](a: Boolean, b: C, c: C) extends plform[A, B, C]
  final case class PlNegFm[A, B, C](a: plform[A, B, C]) extends plform[A, B, C]
  final case class PlBinopFm[A, B, C](a: binop, b: plform[A, B, C], c: plform[A, B, C])
    extends plform[A, B, C]
  final case class PlQuantifFm[C, A, B](a: quantif, b: List[String], c: plform[A, B, C])
    extends plform[A, B, C]

  abstract sealed class rule_rep[A, B, C]
  final case class Clash_rep[A, B, C]() extends rule_rep[A, B, C]
  final case class ConjCRule_rep[A, B, C](a: form[A, B, C]) extends rule_rep[A, B, C]
  final case class DisjCRule_rep[A, B, C](a: form[A, B, C]) extends rule_rep[A, B, C]
  final case class AllCRule_rep[A, B, C](a: form[A, B, C]) extends rule_rep[A, B, C]
  final case class SomeCRule_rep[C, A, B](a: C, b: form[A, B, C]) extends rule_rep[A, B, C]
  final case class SubstCRule_rep[A, B, C](a: form[A, B, C]) extends rule_rep[A, B, C]
  final case class EqRule_rep[A, B, C](a: form[A, B, C]) extends rule_rep[A, B, C]
  final case class ConjFmRule_rep[A, B, C](a: form[A, B, C]) extends rule_rep[A, B, C]
  final case class DisjFmRule_rep[A, B, C](a: form[A, B, C]) extends rule_rep[A, B, C]
  final case class SubstFmRule_rep[A, B, C](a: form[A, B, C]) extends rule_rep[A, B, C]
  final case class ChooseRule_rep[A, B, C](a: form[A, B, C]) extends rule_rep[A, B, C]
  final case class NumrestrC_GeRule_rep[A, B, C](a: form[A, B, C]) extends rule_rep[A, B, C]
  final case class NumrestrC_Lt1Rule_rep[A, B, C](a: form[A, B, C]) extends rule_rep[A, B, C]
  final case class NumrestrC_LtRule_rep[C, A, B](a: List[C], b: form[A, B, C])
    extends rule_rep[A, B, C]
  final case class Todo_rep[A, B, C]() extends rule_rep[A, B, C]

  abstract sealed class trace[A, B, C]
  final case class Trace[A, B, C](a: rule_rep[A, B, C], b: List[trace[A, B, C]])
    extends trace[A, B, C]

  abstract sealed class inactive_form[A, B, C]
  final case class Inactive_form[A, B, C](a: (List[form[A, B, C]], (List[form[A, B, C]], (List[form[A, B, C]], (List[form[A, B, C]], List[form[A, B, C]])))))
      extends inactive_form[A, B, C]

  abstract sealed class branch[A, B, C]
  final case class Branch[A, B, C](a: (List[form[A, B, C]], (List[form[A, B, C]], (List[form[A, B, C]], (List[form[A, B, C]], inactive_form[A, B, C])))))
      extends branch[A, B, C]

  abstract sealed class apply_result[A, B, C]
  final case class AppRes[A, B, C](a: nat, b: Option[rule_rep[A, B, C]], c: List[branch[A, B, C]])
    extends apply_result[A, B, C]

  abstract sealed class proof_result[A, B]
  final case class TablUnsatisf[A, B](a: A) extends proof_result[A, B]
  final case class TablSatisf[B, A](a: B) extends proof_result[A, B]
  final case class InternalError[A, B]() extends proof_result[A, B]

  abstract sealed class trace_constr[A, B, C]
  final case class CTrace[A, B, C](a: trace[A, B, C]) extends trace_constr[A, B, C]
  final case class ITrace[A, B, C](a: List[trace[A, B, C]], b: nat, c: rule_rep[A, B, C])
    extends trace_constr[A, B, C]

  abstract sealed class Interp_impl_ext[A, B, C, D]
  final case class Interp_impl_exta[C, B, A, D](a: List[C], b: List[B], c: List[A], d: B => List[C],
    e: A => List[(C, C)], f: C => List[(Boolean, B)], g: D)
      extends Interp_impl_ext[A, B, C, D]

  def id[A]: A => A = ((x: A) => x)

  def Suc(n: nat): nat = plus_nat(n, one_nat)

  def apply_var_subst[A, B, C, D](f: A => B, x1: subst[C, D, A]): subst[C, D, B] =
    (f, x1) match {
      case (f, RSubst(r, rop, (v1, v2))) => RSubst[C, B, D](r, rop, (f(v1), f(v2)))
      case (f, CSubst(c, rop, v)) => CSubst[D, B, C](c, rop, f(v))
    }

  def apply_var_concept[A, B, C, D](f: A => B, x1: concept[C, D, A]): concept[C, D, B] =
    (f, x1) match {
      case (f, Bottom()) => Bottom[C, D, B]()
      case (f, Top()) => Top[C, D, B]()
      case (f, AtomC(sign, a)) => AtomC[D, C, B](sign, a)
      case (f, BinopC(bop, c1, c2)) =>
        BinopC[C, D, B](bop, apply_var_concept[A, B, C, D](f, c1),
          apply_var_concept[A, B, C, D](f, c2))
      case (f, NegC(c)) => NegC[C, D, B](apply_var_concept[A, B, C, D](f, c))
      case (f, NumRestrC(nro, nb, r, c)) =>
        NumRestrC[C, D, B](nro, nb, r, apply_var_concept[A, B, C, D](f, c))
      case (f, SubstC(c, sb)) =>
        SubstC[C, D, B](apply_var_concept[A, B, C, D](f, c),
          apply_var_subst[A, B, C, D](f, sb))
      case (f, SomeC(r, c)) =>
        SomeC[C, D, B](r, apply_var_concept[A, B, C, D](f, c))
      case (f, AllC(r, c)) => AllC[C, D, B](r, apply_var_concept[A, B, C, D](f, c))
    }

  def apply_var_fact[A, B, C, D](f: A => B, x1: fact[C, D, A]): fact[C, D, B] =
    (f, x1) match {
      case (f, Inst(x, c)) =>
        Inst[B, C, D](f(x), apply_var_concept[A, B, C, D](f, c))
      case (f, AtomR(sign, r, x, y)) => AtomR[C, B, D](sign, r, f(x), f(y))
      case (f, Eq(sign, x, y)) => Eq[B, C, D](sign, f(x), f(y))
    }

  def qform_of_form[A, B, C](x0: form[A, B, C]): qform[A, B, C] = x0 match {
    case ConstFm(cn) => QConstFm[A, B, C](cn)
    case FactFm(fct) =>
      QFactFm[A, B, C](apply_var_fact[C, vara[C], A, B](((a: C) => Free[C](a)), fct))
    case NegFm(f) => QNegFm[A, B, C](qform_of_form[A, B, C](f))
    case BinopFm(bop, f1, f2) =>
      QBinopFm[A, B, C](bop, qform_of_form[A, B, C](f1), qform_of_form[A, B, C](f2))
    case SubstFm(f, sb) =>
      QSubstFm[A, B, C](qform_of_form[A, B, C](f),
        apply_var_subst[C, vara[C], A, B](((a: C) => Free[C](a)), sb))
  }

  def QIfThenElseFm[A, B, C](c: qform[A, B, C], a: qform[A, B, C], b: qform[A, B, C]): qform[A, B, C] =
    QBinopFm[A, B, C](Conj(), QBinopFm[A, B, C](Disj(), QNegFm[A, B, C](c), a),
      QBinopFm[A, B, C](Disj(), QNegFm[A, B, C](QNegFm[A, B, C](c)), b))

  def zero_nat: nat = Nat(BigInt(0))

  def lift_var[A](n: nat, x1: vara[A]): vara[A] = (n, x1) match {
    case (n, Free(v)) => Free[A](v)
    case (n, Bound(k)) =>
      (if (less_eq_nat(n, k)) Bound[A](plus_nat(k, one_nat)) else Bound[A](k))
  }

  def lift_rename[A](n: nat, x1: rename[vara[A]]): rename[vara[A]] = (n, x1) match {
    case (n, ISubst(va, v)) =>
      ISubst[vara[A]](lift_var[A](n, va), lift_var[A](n, v))
  }

  def lift_subst[A, B, C](n: nat, x1: subst[A, B, vara[C]]): subst[A, B, vara[C]] =
    (n, x1) match {
      case (n, RSubst(r, rop, (v1, v2))) =>
        RSubst[A, vara[C], B](r, rop, (lift_var[C](n, v1), lift_var[C](n, v2)))
      case (n, CSubst(c, rop, v)) =>
        CSubst[B, vara[C], A](c, rop, lift_var[C](n, v))
    }

  def lift_concept[A, B, C](n: nat, x1: concept[A, B, vara[C]]): concept[A, B, vara[C]] =
    (n, x1) match {
      case (n, Bottom()) => Bottom[A, B, vara[C]]()
      case (n, Top()) => Top[A, B, vara[C]]()
      case (n, AtomC(sign, a)) => AtomC[B, A, vara[C]](sign, a)
      case (n, BinopC(bop, c1, c2)) =>
        BinopC[A, B, vara[C]](bop, lift_concept[A, B, C](n, c1),
          lift_concept[A, B, C](n, c2))
      case (n, NegC(c)) => NegC[A, B, vara[C]](lift_concept[A, B, C](n, c))
      case (n, NumRestrC(nro, nb, r, c)) =>
        NumRestrC[A, B, vara[C]](nro, nb, r, lift_concept[A, B, C](n, c))
      case (n, SubstC(c, sb)) =>
        SubstC[A, B, vara[C]](lift_concept[A, B, C](n, c), lift_subst[A, B, C](n, sb))
      case (n, SomeC(r, c)) => SomeC[A, B, vara[C]](r, lift_concept[A, B, C](n, c))
      case (n, AllC(r, c)) => AllC[A, B, vara[C]](r, lift_concept[A, B, C](n, c))
    }

  def lift_fact[A, B, C](n: nat, x1: fact[A, B, vara[C]]): fact[A, B, vara[C]] =
    (n, x1) match {
      case (n, Inst(x, c)) =>
        Inst[vara[C], A, B](lift_var[C](n, x), lift_concept[A, B, C](n, c))
      case (n, AtomR(sign, r, x, y)) =>
        AtomR[A, vara[C], B](sign, r, lift_var[C](n, x), lift_var[C](n, y))
      case (n, Eq(sign, x, y)) =>
        Eq[vara[C], A, B](sign, lift_var[C](n, x), lift_var[C](n, y))
    }

  def lift_form[A, B, C](n: nat, x1: qform[A, B, C]): qform[A, B, C] = (n, x1) match {
    case (n, QConstFm(cn)) => QConstFm[A, B, C](cn)
    case (n, QFactFm(fct)) => QFactFm[A, B, C](lift_fact[A, B, C](n, fct))
    case (n, QNegFm(f)) => QNegFm[A, B, C](lift_form[A, B, C](n, f))
    case (n, QBinopFm(bop, f1, f2)) =>
      QBinopFm[A, B, C](bop, lift_form[A, B, C](n, f1), lift_form[A, B, C](n, f2))
    case (n, QuantifFm(q, v, f)) =>
      QuantifFm[C, A, B](q, v, lift_form[A, B, C](plus_nat(n, one_nat), f))
    case (n, QSubstFm(f, sb)) =>
      QSubstFm[A, B, C](lift_form[A, B, C](n, f), lift_subst[A, B, C](n, sb))
    case (n, QRenameFm(f, sb)) =>
      QRenameFm[A, B, C](lift_form[A, B, C](n, f), lift_rename[C](n, sb))
  }

  def bind[A, B, C](q: quantif, v: A, f: qform[B, C, A]): qform[B, C, A] =
    QuantifFm[A, B, C](q, v,
      QRenameFm[B, C, A](lift_form[B, C, A](zero_nat, f),
        ISubst[vara[A]](Free[A](v),
          Bound[A](zero_nat))))

  def foldr[A, B](f: A => B => B, x1: List[A]): B => B = (f, x1) match {
    case (f, Nil) => id[B]
    case (f, x :: xs) => comp[B, B, B](f(x), foldr[A, B](f, xs))
  }

  def bind_list[A, B, C]: quantif => (List[A]) => (qform[B, C, A]) => qform[B, C, A] =
    comp[A => (qform[B, C, A]) => qform[B, C, A], (List[A]) => (qform[B, C, A]) => qform[B, C, A], quantif](((a: A => (qform[B, C, A]) => qform[B, C, A]) =>
      (b: List[A]) => foldr[A, qform[B, C, A]](a, b)),
      ((a: quantif) => (b: A) => (c: qform[B, C, A]) =>
        bind[A, B, C](a, b, c)))

  def wp_dl[A, B, C](x0: stmt[A, B, C], qd: qform[A, B, C]): qform[A, B, C] =
    (x0, qd) match {
      case (Skip(), qd) => qd
      case (NAdd(v, c), qd) =>
        QSubstFm[A, B, C](qd, CSubst[B, vara[C], A](c, SAdd(), Free[C](v)))
      case (NDel(v, c), qd) =>
        QSubstFm[A, B, C](qd, CSubst[B, vara[C], A](c, SDiff(), Free[C](v)))
      case (EAdd(v1, r, v2), qd) =>
        QSubstFm[A, B, C](qd, RSubst[A, vara[C], B](r, SAdd(), (Free[C](v1), Free[C](v2))))
      case (EDel(v1, r, v2), qd) =>
        QSubstFm[A, B, C](qd, RSubst[A, vara[C], B](r, SDiff(), (Free[C](v1), Free[C](v2))))
      case (SelAss(vs, b), qd) =>
        bind_list[C, A, B].apply(QAll()).apply(vs).apply(QBinopFm[A, B, C](Disj(), QNegFm[A, B, C](qform_of_form[A, B, C](b)), qd))
      case (Seq(c_1, c_2), qd) => wp_dl[A, B, C](c_1, wp_dl[A, B, C](c_2, qd))
      case (If(b, c_1, c_2), qd) =>
        QIfThenElseFm[A, B, C](qform_of_form[A, B, C](b), wp_dl[A, B, C](c_1, qd),
          wp_dl[A, B, C](c_2, qd))
      case (While(iv, b, c), qd) => qform_of_form[A, B, C](iv)
    }

  def vc[A, B, C](x0: stmt[A, B, C], qd: qform[A, B, C]): qform[A, B, C] =
    (x0, qd) match {
      case (Skip(), qd) => QConstFm[A, B, C](true)
      case (NAdd(v, c), qd) => QConstFm[A, B, C](true)
      case (NDel(v, c), qd) => QConstFm[A, B, C](true)
      case (EAdd(v1, r, v2), qd) => QConstFm[A, B, C](true)
      case (EDel(v1, r, v2), qd) => QConstFm[A, B, C](true)
      case (SelAss(vs, b), qd) => QConstFm[A, B, C](true)
      case (Seq(c_1, c_2), qd) =>
        QBinopFm[A, B, C](Conj(), vc[A, B, C](c_1, wp_dl[A, B, C](c_2, qd)),
          vc[A, B, C](c_2, qd))
      case (If(b, c_1, c_2), qd) =>
        QBinopFm[A, B, C](Conj(), vc[A, B, C](c_1, qd), vc[A, B, C](c_2, qd))
      case (While(iv, b, c), qd) =>
        {
          val qb: qform[A, B, C] = qform_of_form[A, B, C](b)
          val qiv: qform[A, B, C] = qform_of_form[A, B, C](iv);
          QBinopFm[A, B, C](Conj(),
            QBinopFm[A, B, C](Conj(),
              QBinopFm[A, B, C](Disj(),
                QNegFm[A, B, C](QBinopFm[A, B, C](Conj(), qiv, QNegFm[A, B, C](qb))),
                qd),
              QBinopFm[A, B, C](Disj(), QNegFm[A, B, C](QBinopFm[A, B, C](Conj(), qiv, qb)),
                wp_dl[A, B, C](c, qiv))),
            vc[A, B, C](c, qiv))
        }
    }

  def fold[A, B](f: A => B => B, x1: List[A], s: B): B = (f, x1, s) match {
    case (f, x :: xs, s) => fold[A, B](f, xs, (f(x))(s))
    case (f, Nil, s) => s
  }

  def rev[A](xs: List[A]): List[A] =
    fold[A, List[A]](((a: A) => (b: List[A]) => a :: b), xs, Nil)

  def upt(i: nat, j: nat): List[nat] =
    (if (less_nat(i, j)) i :: upt(Suc(i), j) else Nil)

  def zip[A, B](xs: List[A], ys: List[B]): List[(A, B)] = (xs, ys) match {
    case (x :: xs, y :: ys) => (x, y) :: zip[A, B](xs, ys)
    case (xs, Nil) => Nil
    case (Nil, ys) => Nil
  }

  def maps[A, B](f: A => List[B], x1: List[A]): List[B] = (f, x1) match {
    case (f, Nil) => Nil
    case (f, x :: xs) => f(x) ++ maps[A, B](f, xs)
  }

  def foldl[A, B](f: A => B => A, a: A, x2: List[B]): A = (f, a, x2) match {
    case (f, a, Nil) => a
    case (f, a, x :: xs) => foldl[A, B](f, (f(a))(x), xs)
  }

  def insert[A: equal](x: A, xs: List[A]): List[A] =
    (if (member[A](xs, x)) xs else x :: xs)

  def union[A: equal]: (List[A]) => (List[A]) => List[A] =
    ((a: List[A]) => (b: List[A]) =>
      fold[A, List[A]](((aa: A) => (ba: List[A]) => insert[A](aa, ba)), a, b))

  def fun_upd[A: equal, B](f: A => B, a: A, b: B): A => B =
    ((x: A) => (if (eq[A](x, a)) b else f(x)))

  def filter[A](p: A => Boolean, x1: List[A]): List[A] = (p, x1) match {
    case (p, Nil) => Nil
    case (p, x :: xs) => (if (p(x)) x :: filter[A](p, xs) else filter[A](p, xs))
  }

  def list_ex[A](p: A => Boolean, x1: List[A]): Boolean = (p, x1) match {
    case (p, Nil) => false
    case (p, x :: xs) => p(x) || list_ex[A](p, xs)
  }

  def remdups[A: equal](x0: List[A]): List[A] = x0 match {
    case Nil => Nil
    case x :: xs =>
      (if (member[A](xs, x)) remdups[A](xs) else x :: remdups[A](xs))
  }

  def pred_list[A](p: A => Boolean, x1: List[A]): Boolean = (p, x1) match {
    case (p, Nil) => true
    case (p, x :: xs) => p(x) && pred_list[A](p, xs)
  }

  def removeAll[A: equal](x: A, xa1: List[A]): List[A] = (x, xa1) match {
    case (x, Nil) => Nil
    case (x, y :: xs) =>
      (if (eq[A](x, y)) removeAll[A](x, xs) else y :: removeAll[A](x, xs))
  }

  def gen_length[A](n: nat, x1: List[A]): nat = (n, x1) match {
    case (n, x :: xs) => gen_length[A](Suc(n), xs)
    case (n, Nil) => n
  }

  def map_filter[A, B](f: A => Option[B], x1: List[A]): List[B] = (f, x1) match {
    case (f, Nil) => Nil
    case (f, x :: xs) => (f(x) match {
      case None => map_filter[A, B](f, xs)
      case Some(y) => y :: map_filter[A, B](f, xs)
    })
  }

  def list_Union[A: equal](xss: List[List[A]]): List[A] =
    (foldr[List[A], List[A]](union[A], xss)).apply(Nil)

  def list_union[A: equal](xs: List[A], ys: List[A]): List[A] =
    remdups[A](xs ++ ys)

  def list_inters[A: equal](xs: List[A], ys: List[A]): List[A] =
    filter[A](((a: A) => member[A](ys, a)), xs)

  def list_remove[A: equal](xs: List[A], ys: List[A]): List[A] =
    (foldr[A, List[A]](((a: A) => (b: List[A]) => removeAll[A](a, b)),
      ys)).apply(xs)

  def extract_vcs[A, B, C](p: qform[A, B, C], c: stmt[A, B, C], q: qform[A, B, C]): qform[A, B, C] =
    QBinopFm[A, B, C](Conj(), vc[A, B, C](c, q),
      QBinopFm[A, B, C](Disj(), QNegFm[A, B, C](p), wp_dl[A, B, C](c, q)))

  def replace_var[A: equal](x0: rename[A], w: A): A = (x0, w) match {
    case (ISubst(v1, v2), w) => (if (eq[A](w, v1)) v2 else w)
  }

  def dual_binop(sign: Boolean, x1: binop): binop = (sign, x1) match {
    case (sign, Conj()) => (if (sign) Conj() else Disj())
    case (sign, Disj()) => (if (sign) Disj() else Conj())
  }

  def extract_subst[A, B, C](x0: fact[A, B, C]): Option[(C, (concept[A, B, C], subst[A, B, C]))] =
    x0 match {
      case Inst(x, SubstC(c, sb)) =>
        Some[(C, (concept[A, B, C], subst[A, B, C]))]((x, (c, sb)))
      case Inst(v, Top()) => None
      case Inst(v, Bottom()) => None
      case Inst(v, AtomC(vb, vc)) => None
      case Inst(v, NegC(vb)) => None
      case Inst(v, BinopC(vb, vc, vd)) => None
      case Inst(v, NumRestrC(vb, vc, vd, ve)) => None
      case Inst(v, SomeC(vb, vc)) => None
      case Inst(v, AllC(vb, vc)) => None
      case AtomR(v, va, vb, vc) => None
      case Eq(v, va, vb) => None
    }

  def rename_in_var[A: equal](x: A, xa1: List[rename[A]]): A = (x, xa1) match {
    case (x, Nil) => x
    case (x, sb :: sbsts) => rename_in_var[A](replace_var[A](sb, x), sbsts)
  }

  def IfThenElseFm[A, B, C](c: form[A, B, C], a: form[A, B, C], b: form[A, B, C]): form[A, B, C] =
    BinopFm[A, B, C](Conj(), BinopFm[A, B, C](Disj(), NegFm[A, B, C](c), a),
      BinopFm[A, B, C](Disj(), NegFm[A, B, C](NegFm[A, B, C](c)), b))

  def dual_quantif(sign: Boolean, x1: quantif): quantif = (sign, x1) match {
    case (sign, QAll()) => (if (sign) QAll() else QEx())
    case (sign, QEx()) => (if (sign) QEx() else QAll())
  }

  def rename_in_subst[A, B, C: equal](x0: subst[A, B, C], sbsts: List[rename[C]]): subst[A, B, C] =
    (x0, sbsts) match {
      case (RSubst(r, rop, (x1, x2)), sbsts) =>
        RSubst[A, C, B](r, rop,
          (rename_in_var[C](x1, sbsts), rename_in_var[C](x2, sbsts)))
      case (CSubst(cr, rop, v), sbsts) =>
        CSubst[B, C, A](cr, rop, rename_in_var[C](v, sbsts))
    }

  def rename_in_concept[A, B, C: equal](x0: concept[A, B, C], sbsts: List[rename[C]]): concept[A, B, C] =
    (x0, sbsts) match {
      case (AtomC(sign, a), sbsts) => AtomC[B, A, C](sign, a)
      case (Top(), sbsts) => Top[A, B, C]()
      case (Bottom(), sbsts) => Bottom[A, B, C]()
      case (NegC(c), sbsts) => NegC[A, B, C](rename_in_concept[A, B, C](c, sbsts))
      case (BinopC(bop, c1, c2), sbsts) =>
        BinopC[A, B, C](bop, rename_in_concept[A, B, C](c1, sbsts),
          rename_in_concept[A, B, C](c2, sbsts))
      case (NumRestrC(nro, n, r, c), sbsts) =>
        NumRestrC[A, B, C](nro, n, r, rename_in_concept[A, B, C](c, sbsts))
      case (SubstC(c, sb), sbsts) =>
        SubstC[A, B, C](rename_in_concept[A, B, C](c, sbsts),
          rename_in_subst[A, B, C](sb, sbsts))
      case (SomeC(r, c), sbsts) =>
        SomeC[A, B, C](r, rename_in_concept[A, B, C](c, sbsts))
      case (AllC(r, c), sbsts) =>
        AllC[A, B, C](r, rename_in_concept[A, B, C](c, sbsts))
    }

  def rename_in_fact[A, B, C: equal](x0: fact[A, B, C], sbsts: List[rename[C]]): fact[A, B, C] =
    (x0, sbsts) match {
      case (Inst(x, c), sbsts) =>
        Inst[C, A, B](rename_in_var[C](x, sbsts), rename_in_concept[A, B, C](c, sbsts))
      case (AtomR(sign, r, x, y), sbsts) =>
        AtomR[A, C, B](sign, r, rename_in_var[C](x, sbsts), rename_in_var[C](y, sbsts))
      case (Eq(sign, x, y), sbsts) =>
        Eq[C, A, B](sign, rename_in_var[C](x, sbsts), rename_in_var[C](y, sbsts))
    }

  def rename_in_form[A, B, C: equal](x0: form[A, B, C], sbsts: List[rename[C]]): form[A, B, C] =
    (x0, sbsts) match {
      case (ConstFm(cn), sbsts) => ConstFm[A, B, C](cn)
      case (FactFm(fct), sbsts) =>
        FactFm[A, B, C](rename_in_fact[A, B, C](fct, sbsts))
      case (NegFm(f), sbsts) => NegFm[A, B, C](rename_in_form[A, B, C](f, sbsts))
      case (BinopFm(bop, f1, f2), sbsts) =>
        BinopFm[A, B, C](bop, rename_in_form[A, B, C](f1, sbsts),
          rename_in_form[A, B, C](f2, sbsts))
      case (SubstFm(f, sb), sbsts) =>
        SubstFm[A, B, C](rename_in_form[A, B, C](f, sbsts),
          rename_in_subst[A, B, C](sb, sbsts))
    }

  def lift_bound_above_renamefm[A, B, C](x0: qform[A, B, C], sb: rename[vara[C]]): qform[A, B, C] =
    (x0, sb) match {
      case (QuantifFm(q, v, f), sb) =>
        QuantifFm[C, A, B](q, v,
          lift_bound_above_renamefm[A, B, C](f, lift_rename[C](zero_nat, sb)))
      case (QConstFm(v), sb) => QRenameFm[A, B, C](QConstFm[A, B, C](v), sb)
      case (QFactFm(v), sb) => QRenameFm[A, B, C](QFactFm[A, B, C](v), sb)
      case (QNegFm(v), sb) => QRenameFm[A, B, C](QNegFm[A, B, C](v), sb)
      case (QBinopFm(v, va, vb), sb) =>
        QRenameFm[A, B, C](QBinopFm[A, B, C](v, va, vb), sb)
      case (QSubstFm(v, va), sb) => QRenameFm[A, B, C](QSubstFm[A, B, C](v, va), sb)
      case (QRenameFm(v, va), sb) =>
        QRenameFm[A, B, C](QRenameFm[A, B, C](v, va), sb)
    }

  def lift_bound_above_substfm[A, B, C](x0: qform[A, B, C], sb: subst[A, B, vara[C]]): qform[A, B, C] =
    (x0, sb) match {
      case (QuantifFm(q, v, f), sb) =>
        QuantifFm[C, A, B](q, v,
          lift_bound_above_substfm[A, B, C](f, lift_subst[A, B, C](zero_nat, sb)))
      case (QConstFm(v), sb) => QSubstFm[A, B, C](QConstFm[A, B, C](v), sb)
      case (QFactFm(v), sb) => QSubstFm[A, B, C](QFactFm[A, B, C](v), sb)
      case (QNegFm(v), sb) => QSubstFm[A, B, C](QNegFm[A, B, C](v), sb)
      case (QBinopFm(v, va, vb), sb) =>
        QSubstFm[A, B, C](QBinopFm[A, B, C](v, va, vb), sb)
      case (QSubstFm(v, va), sb) => QSubstFm[A, B, C](QSubstFm[A, B, C](v, va), sb)
      case (QRenameFm(v, va), sb) =>
        QSubstFm[A, B, C](QRenameFm[A, B, C](v, va), sb)
    }

  def lift_bound_above_negfm[A, B, C](x0: qform[A, B, C]): qform[A, B, C] = x0 match {
    case QuantifFm(q, v, f) =>
      QuantifFm[C, A, B](dual_quantif(false, q), v, lift_bound_above_negfm[A, B, C](f))
    case QConstFm(v) => QNegFm[A, B, C](QConstFm[A, B, C](v))
    case QFactFm(v) => QNegFm[A, B, C](QFactFm[A, B, C](v))
    case QNegFm(v) => QNegFm[A, B, C](QNegFm[A, B, C](v))
    case QBinopFm(v, va, vb) => QNegFm[A, B, C](QBinopFm[A, B, C](v, va, vb))
    case QSubstFm(v, va) => QNegFm[A, B, C](QSubstFm[A, B, C](v, va))
    case QRenameFm(v, va) => QNegFm[A, B, C](QRenameFm[A, B, C](v, va))
  }

  def shuffle_right[A, B, C](bop: binop, f1: qform[A, B, C], x2: qform[A, B, C]): qform[A, B, C] =
    (bop, f1, x2) match {
      case (bop, f1, QuantifFm(q, v, f2)) =>
        QuantifFm[C, A, B](q, v,
          shuffle_right[A, B, C](bop, lift_form[A, B, C](zero_nat, f1), f2))
      case (bop, f1, QConstFm(v)) =>
        QBinopFm[A, B, C](bop, f1, QConstFm[A, B, C](v))
      case (bop, f1, QFactFm(v)) => QBinopFm[A, B, C](bop, f1, QFactFm[A, B, C](v))
      case (bop, f1, QNegFm(v)) => QBinopFm[A, B, C](bop, f1, QNegFm[A, B, C](v))
      case (bop, f1, QBinopFm(v, va, vb)) =>
        QBinopFm[A, B, C](bop, f1, QBinopFm[A, B, C](v, va, vb))
      case (bop, f1, QSubstFm(v, va)) =>
        QBinopFm[A, B, C](bop, f1, QSubstFm[A, B, C](v, va))
      case (bop, f1, QRenameFm(v, va)) =>
        QBinopFm[A, B, C](bop, f1, QRenameFm[A, B, C](v, va))
    }

  def shuffle_left[A, B, C](bop: binop, x1: qform[A, B, C], f2: qform[A, B, C]): qform[A, B, C] =
    (bop, x1, f2) match {
      case (bop, QuantifFm(q, v, f1), f2) =>
        QuantifFm[C, A, B](q, v,
          shuffle_left[A, B, C](bop, f1, lift_form[A, B, C](zero_nat, f2)))
      case (bop, QConstFm(v), f2) =>
        shuffle_right[A, B, C](bop, QConstFm[A, B, C](v), f2)
      case (bop, QFactFm(v), f2) =>
        shuffle_right[A, B, C](bop, QFactFm[A, B, C](v), f2)
      case (bop, QNegFm(v), f2) =>
        shuffle_right[A, B, C](bop, QNegFm[A, B, C](v), f2)
      case (bop, QBinopFm(v, va, vb), f2) =>
        shuffle_right[A, B, C](bop, QBinopFm[A, B, C](v, va, vb), f2)
      case (bop, QSubstFm(v, va), f2) =>
        shuffle_right[A, B, C](bop, QSubstFm[A, B, C](v, va), f2)
      case (bop, QRenameFm(v, va), f2) =>
        shuffle_right[A, B, C](bop, QRenameFm[A, B, C](v, va), f2)
    }

  def lift_bound[A, B, C](x0: qform[A, B, C]): qform[A, B, C] = x0 match {
    case QConstFm(cn) => QConstFm[A, B, C](cn)
    case QFactFm(fct) => QFactFm[A, B, C](fct)
    case QNegFm(f) => lift_bound_above_negfm[A, B, C](lift_bound[A, B, C](f))
    case QBinopFm(bop, f1, f2) =>
      shuffle_left[A, B, C](bop, lift_bound[A, B, C](f1), lift_bound[A, B, C](f2))
    case QuantifFm(q, v, f) => QuantifFm[C, A, B](q, v, lift_bound[A, B, C](f))
    case QSubstFm(f, sb) =>
      lift_bound_above_substfm[A, B, C](lift_bound[A, B, C](f), sb)
    case QRenameFm(f, sb) =>
      lift_bound_above_renamefm[A, B, C](lift_bound[A, B, C](f), sb)
  }

  def fst[A, B](x0: (A, B)): A = x0 match {
    case (x1, x2) => x1
  }

  def minus_nat(m: nat, n: nat): nat =
    Nat(max[BigInt](BigInt(0), integer_of_nat(m) - integer_of_nat(n)))

  def dual_numres_ord(sign: Boolean, x1: numres_ord): numres_ord = (sign, x1) match {
    case (sign, Lt()) => (if (sign) Lt() else Ge())
    case (sign, Ge()) => (if (sign) Ge() else Lt())
  }

  def interp_sign(sign: Boolean, b: Boolean): Boolean = (if (sign) b else !b)

  def neg_norm_concept[A, B, C](sign: Boolean, x1: concept[A, B, C]): concept[A, B, C] =
    (sign, x1) match {
      case (sign, Bottom()) => (if (sign) Bottom[A, B, C]() else Top[A, B, C]())
      case (sign, Top()) => (if (sign) Top[A, B, C]() else Bottom[A, B, C]())
      case (sign, AtomC(asign, a)) => AtomC[B, A, C](interp_sign(sign, asign), a)
      case (sign, BinopC(bop, c1, c2)) =>
        BinopC[A, B, C](dual_binop(sign, bop), neg_norm_concept[A, B, C](sign, c1),
          neg_norm_concept[A, B, C](sign, c2))
      case (sign, NegC(c)) => neg_norm_concept[A, B, C](!sign, c)
      case (sign, NumRestrC(nro, n, r, c)) =>
        NumRestrC[A, B, C](dual_numres_ord(sign, nro), n, r,
          neg_norm_concept[A, B, C](true, c))
      case (sign, SubstC(c, sb)) =>
        SubstC[A, B, C](neg_norm_concept[A, B, C](sign, c), sb)
      case (sign, SomeC(r, c)) =>
        (if (sign) SomeC[A, B, C](r, neg_norm_concept[A, B, C](sign, c))
        else AllC[A, B, C](r, neg_norm_concept[A, B, C](sign, c)))
      case (sign, AllC(r, c)) =>
        (if (sign) AllC[A, B, C](r, neg_norm_concept[A, B, C](sign, c))
        else SomeC[A, B, C](r, neg_norm_concept[A, B, C](sign, c)))
    }

  def neg_norm_fact[A, B, C](sign: Boolean, x1: fact[A, B, C]): fact[A, B, C] =
    (sign, x1) match {
      case (sign, Inst(x, c)) =>
        Inst[C, A, B](x, neg_norm_concept[A, B, C](sign, c))
      case (signa, AtomR(sign, r, x, y)) =>
        AtomR[A, C, B](interp_sign(signa, sign), r, x, y)
      case (signa, Eq(sign, x, y)) => Eq[C, A, B](interp_sign(signa, sign), x, y)
    }

  def neg_norm_form[A, B, C](sign: Boolean, x1: form[A, B, C]): form[A, B, C] =
    (sign, x1) match {
      case (sign, ConstFm(cn)) => ConstFm[A, B, C](interp_sign(sign, cn))
      case (sign, FactFm(f)) => FactFm[A, B, C](neg_norm_fact[A, B, C](sign, f))
      case (sign, NegFm(f)) => neg_norm_form[A, B, C](!sign, f)
      case (sign, BinopFm(bop, f1, f2)) =>
        BinopFm[A, B, C](dual_binop(sign, bop), neg_norm_form[A, B, C](sign, f1),
          neg_norm_form[A, B, C](sign, f2))
      case (sign, SubstFm(f, sb)) =>
        SubstFm[A, B, C](neg_norm_form[A, B, C](sign, f), sb)
    }

  def nnf_IfThenElseFm[A, B, C](c: form[A, B, C], a: form[A, B, C], b: form[A, B, C]): form[A, B, C] =
    neg_norm_form[A, B, C](true, IfThenElseFm[A, B, C](c, a, b))

  def push_rsubst_concept_numrestrc[A, B, C](xa0: subst_op, xa1: (A, A), x: A,
    nro: numres_ord, n: nat, r: B,
    c: concept[B, C, A]): form[B, C, A] =
    (xa0, xa1, x, nro, n, r, c) match {
      case (SDiff(), (v1, v2), x, nro, n, r, c) =>
        nnf_IfThenElseFm[B, C, A](BinopFm[B, C, A](Conj(),
          BinopFm[B, C, A](Conj(), FactFm[B, C, A](Eq[A, B, C](true, x, v1)),
            FactFm[B, C, A](Inst[A, B, C](v2, SubstC[B, C, A](c, RSubst[B, A, C](r, SDiff(), (v1, v2)))))),
          FactFm[B, C, A](AtomR[B, A, C](true, r, v1, v2))),
          FactFm[B, C, A](Inst[A, B, C](x, NumRestrC[B, C, A](nro, Suc(n), r,
            SubstC[B, C, A](c, RSubst[B, A, C](r, SDiff(), (v1, v2)))))),
          FactFm[B, C, A](Inst[A, B, C](x, NumRestrC[B, C, A](nro, n, r,
            SubstC[B, C, A](c, RSubst[B, A, C](r, SDiff(), (v1, v2)))))))
      case (SAdd(), (v1, v2), x, Lt(), n, r, c) =>
        (if (equal_nat(n, zero_nat)) ConstFm[B, C, A](false)
        else nnf_IfThenElseFm[B, C, A](BinopFm[B, C, A](Conj(),
          BinopFm[B, C, A](Conj(), FactFm[B, C, A](Eq[A, B, C](true, x, v1)),
            FactFm[B, C, A](Inst[A, B, C](v2, SubstC[B, C, A](c, RSubst[B, A, C](r, SAdd(), (v1, v2)))))),
          FactFm[B, C, A](AtomR[B, A, C](false, r, v1, v2))),
          FactFm[B, C, A](Inst[A, B, C](x, NumRestrC[B, C, A](Lt(), minus_nat(n, one_nat), r,
            SubstC[B, C, A](c,
              RSubst[B, A, C](r, SAdd(), (v1, v2)))))),
          FactFm[B, C, A](Inst[A, B, C](x, NumRestrC[B, C, A](Lt(), Suc(minus_nat(n, one_nat)), r,
            SubstC[B, C, A](c,
              RSubst[B, A, C](r, SAdd(), (v1, v2))))))))
      case (SAdd(), (v1, v2), x, Ge(), n, r, c) =>
        (if (equal_nat(n, zero_nat)) ConstFm[B, C, A](true)
        else nnf_IfThenElseFm[B, C, A](BinopFm[B, C, A](Conj(),
          BinopFm[B, C, A](Conj(), FactFm[B, C, A](Eq[A, B, C](true, x, v1)),
            FactFm[B, C, A](Inst[A, B, C](v2, SubstC[B, C, A](c, RSubst[B, A, C](r, SAdd(), (v1, v2)))))),
          FactFm[B, C, A](AtomR[B, A, C](false, r, v1, v2))),
          FactFm[B, C, A](Inst[A, B, C](x, NumRestrC[B, C, A](Ge(), minus_nat(n, one_nat), r,
            SubstC[B, C, A](c,
              RSubst[B, A, C](r, SAdd(), (v1, v2)))))),
          FactFm[B, C, A](Inst[A, B, C](x, NumRestrC[B, C, A](Ge(), Suc(minus_nat(n, one_nat)), r,
            SubstC[B, C, A](c,
              RSubst[B, A, C](r, SAdd(), (v1, v2))))))))
    }

  def push_rsubst_concept_somec[A, B, C](xa0: subst_op, xa1: (A, A), x: A, r: B,
    c: concept[B, C, A]): form[B, C, A] =
    (xa0, xa1, x, r, c) match {
      case (SDiff(), (v1, v2), x, r, c) =>
        nnf_IfThenElseFm[B, C, A](BinopFm[B, C, A](Conj(),
          BinopFm[B, C, A](Conj(), FactFm[B, C, A](Eq[A, B, C](true, x, v1)),
            FactFm[B, C, A](Inst[A, B, C](v2, SubstC[B, C, A](c, RSubst[B, A, C](r, SDiff(), (v1, v2)))))),
          FactFm[B, C, A](AtomR[B, A, C](true, r, v1, v2))),
          FactFm[B, C, A](Inst[A, B, C](x, NumRestrC[B, C, A](Ge(), Suc(Suc(zero_nat)), r,
            SubstC[B, C, A](c, RSubst[B, A, C](r, SDiff(), (v1, v2)))))),
          FactFm[B, C, A](Inst[A, B, C](x, SomeC[B, C, A](r, SubstC[B, C, A](c, RSubst[B, A, C](r, SDiff(), (v1, v2)))))))
      case (SAdd(), (v1, v2), x, r, c) =>
        nnf_IfThenElseFm[B, C, A](BinopFm[B, C, A](Conj(),
          BinopFm[B, C, A](Conj(), FactFm[B, C, A](Eq[A, B, C](true, x, v1)),
            FactFm[B, C, A](Inst[A, B, C](v2, SubstC[B, C, A](c, RSubst[B, A, C](r, SAdd(), (v1, v2)))))),
          FactFm[B, C, A](AtomR[B, A, C](false, r, v1, v2))),
          ConstFm[B, C, A](true),
          FactFm[B, C, A](Inst[A, B, C](x, SomeC[B, C, A](r, SubstC[B, C, A](c, RSubst[B, A, C](r, SAdd(), (v1, v2)))))))
    }

  def push_rsubst_concept_allc[A, B, C](xa0: subst_op, xa1: (A, A), x: A, r: B,
    c: concept[B, C, A]): form[B, C, A] =
    (xa0, xa1, x, r, c) match {
      case (SDiff(), (v1, v2), x, r, c) =>
        nnf_IfThenElseFm[B, C, A](BinopFm[B, C, A](Conj(),
          BinopFm[B, C, A](Conj(), FactFm[B, C, A](Eq[A, B, C](true, x, v1)),
            FactFm[B, C, A](Inst[A, B, C](v2, NegC[B, C, A](SubstC[B, C, A](c, RSubst[B, A, C](r, SDiff(), (v1, v2))))))),
          FactFm[B, C, A](AtomR[B, A, C](true, r, v1, v2))),
          FactFm[B, C, A](Inst[A, B, C](x, NumRestrC[B, C, A](Lt(), Suc(one_nat), r,
            NegC[B, C, A](SubstC[B, C, A](c, RSubst[B, A, C](r, SDiff(), (v1, v2))))))),
          FactFm[B, C, A](Inst[A, B, C](x, AllC[B, C, A](r, SubstC[B, C, A](c, RSubst[B, A, C](r, SDiff(), (v1, v2)))))))
      case (SAdd(), (v1, v2), x, r, c) =>
        nnf_IfThenElseFm[B, C, A](BinopFm[B, C, A](Conj(),
          BinopFm[B, C, A](Conj(), FactFm[B, C, A](Eq[A, B, C](true, x, v1)),
            FactFm[B, C, A](Inst[A, B, C](v2, NegC[B, C, A](SubstC[B, C, A](c, RSubst[B, A, C](r, SAdd(), (v1, v2))))))),
          FactFm[B, C, A](AtomR[B, A, C](false, r, v1, v2))),
          ConstFm[B, C, A](false),
          FactFm[B, C, A](Inst[A, B, C](x, AllC[B, C, A](r, SubstC[B, C, A](c, RSubst[B, A, C](r, SAdd(), (v1, v2)))))))
    }

  def push_rsubst_concept[A: equal, B, C](r: A, rop: subst_op, v1v2: (B, B), x: B,
    xa4: concept[A, C, B]): form[A, C, B] =
    (r, rop, v1v2, x, xa4) match {
      case (r, rop, v1v2, x, AtomC(sign, a)) =>
        FactFm[A, C, B](Inst[B, A, C](x, AtomC[C, A, B](sign, a)))
      case (r, rop, v1v2, x, Top()) =>
        FactFm[A, C, B](Inst[B, A, C](x, Top[A, C, B]()))
      case (r, rop, v1v2, x, Bottom()) =>
        FactFm[A, C, B](Inst[B, A, C](x, Bottom[A, C, B]()))
      case (r, rop, v1v2, x, NegC(c)) =>
        FactFm[A, C, B](Inst[B, A, C](x, NegC[A, C, B](SubstC[A, C, B](c, RSubst[A, B, C](r, rop, v1v2)))))
      case (r, rop, v1v2, x, BinopC(bop, c1, c2)) =>
        FactFm[A, C, B](Inst[B, A, C](x, BinopC[A, C, B](bop, SubstC[A, C, B](c1, RSubst[A, B, C](r, rop, v1v2)),
          SubstC[A, C, B](c2, RSubst[A, B, C](r, rop, v1v2)))))
      case (ra, rop, v1v2, x, NumRestrC(nro, n, r, c)) =>
        (if (eq[A](ra, r))
          push_rsubst_concept_numrestrc[B, A, C](rop, v1v2, x, nro, n, ra, c)
        else FactFm[A, C, B](Inst[B, A, C](x, NumRestrC[A, C, B](nro, n, r, SubstC[A, C, B](c, RSubst[A, B, C](ra, rop, v1v2))))))
      case (r, rop, v1v2, x, SubstC(c, sb)) =>
        SubstFm[A, C, B](FactFm[A, C, B](Inst[B, A, C](x, SubstC[A, C, B](c, sb))),
          RSubst[A, B, C](r, rop, v1v2))
      case (ra, rop, v1v2, x, SomeC(r, c)) =>
        (if (eq[A](ra, r)) push_rsubst_concept_somec[B, A, C](rop, v1v2, x, ra, c)
        else FactFm[A, C, B](Inst[B, A, C](x, SomeC[A, C, B](r, SubstC[A, C, B](c, RSubst[A, B, C](ra, rop, v1v2))))))
      case (ra, rop, v1v2, x, AllC(r, c)) =>
        (if (eq[A](ra, r)) push_rsubst_concept_allc[B, A, C](rop, v1v2, x, ra, c)
        else FactFm[A, C, B](Inst[B, A, C](x, AllC[A, C, B](r, SubstC[A, C, B](c, RSubst[A, B, C](ra, rop, v1v2))))))
    }

  def subst_AtomR_SDiff[A, B, C](sign: Boolean, r: A, x: B, y: B, v1: B, v2: B): form[A, C, B] =
    neg_norm_form[A, C, B](sign,
      BinopFm[A, C, B](Conj(),
        BinopFm[A, C, B](Disj(), FactFm[A, C, B](Eq[B, A, C](false, x, v1)),
          FactFm[A, C, B](Eq[B, A, C](false, y, v2))),
        FactFm[A, C, B](AtomR[A, B, C](true, r, x, y))))

  def subst_AtomR_SAdd[A, B, C](sign: Boolean, r: A, x: B, y: B, v1: B, v2: B): form[A, C, B] =
    neg_norm_form[A, C, B](sign,
      BinopFm[A, C, B](Disj(),
        BinopFm[A, C, B](Conj(), FactFm[A, C, B](Eq[B, A, C](true, x, v1)),
          FactFm[A, C, B](Eq[B, A, C](true, y, v2))),
        FactFm[A, C, B](AtomR[A, B, C](true, r, x, y))))

  def push_rsubst_fact[A: equal, B, C](r: A, rop: subst_op, v1v2: (B, B), x3: fact[A, C, B]): form[A, C, B] =
    (r, rop, v1v2, x3) match {
      case (r, rop, v1v2, Inst(x, c)) =>
        push_rsubst_concept[A, B, C](r, rop, v1v2, x, c)
      case (ra, rop, v1v2, AtomR(sign, r, x, y)) =>
        (if (eq[A](ra, r)) {
          val (v1, v2): (B, B) = v1v2;
          (rop match {
            case SDiff() => subst_AtomR_SDiff[A, B, C](sign, ra, x, y, v1, v2)
            case SAdd() => subst_AtomR_SAdd[A, B, C](sign, ra, x, y, v1, v2)
          })
        } else FactFm[A, C, B](AtomR[A, B, C](sign, r, x, y)))
      case (r, rop, v1v2, Eq(sign, x, y)) =>
        FactFm[A, C, B](Eq[B, A, C](sign, x, y))
    }

  def push_csubst_concept[A: equal, B, C](cr: A, rop: subst_op, v: B, x: B,
    xa4: concept[C, A, B]): form[C, A, B] =
    (cr, rop, v, x, xa4) match {
      case (cr, rop, v, x, AtomC(sign, a)) =>
        (if (eq[A](cr, a))
          (rop match {
          case SDiff() =>
            neg_norm_form[C, A, B](sign,
              BinopFm[C, A, B](Conj(),
                FactFm[C, A, B](Inst[B, C, A](x, AtomC[A, C, B](true, a))),
                FactFm[C, A, B](Eq[B, C, A](false, x, v))))
          case SAdd() =>
            neg_norm_form[C, A, B](sign,
              BinopFm[C, A, B](Disj(),
                FactFm[C, A, B](Inst[B, C, A](x, AtomC[A, C, B](true, a))),
                FactFm[C, A, B](Eq[B, C, A](true, x, v))))
        })
        else FactFm[C, A, B](Inst[B, C, A](x, AtomC[A, C, B](sign, a))))
      case (cr, rop, v, x, Top()) =>
        FactFm[C, A, B](Inst[B, C, A](x, Top[C, A, B]()))
      case (cr, rop, v, x, Bottom()) =>
        FactFm[C, A, B](Inst[B, C, A](x, Bottom[C, A, B]()))
      case (cr, rop, v, x, NegC(c)) =>
        FactFm[C, A, B](Inst[B, C, A](x, NegC[C, A, B](SubstC[C, A, B](c, CSubst[A, B, C](cr, rop, v)))))
      case (cr, rop, v, x, BinopC(bop, c1, c2)) =>
        FactFm[C, A, B](Inst[B, C, A](x, BinopC[C, A, B](bop, SubstC[C, A, B](c1, CSubst[A, B, C](cr, rop, v)),
          SubstC[C, A, B](c2, CSubst[A, B, C](cr, rop, v)))))
      case (cr, rop, v, x, NumRestrC(nro, n, r, c)) =>
        FactFm[C, A, B](Inst[B, C, A](x, NumRestrC[C, A, B](nro, n, r,
          SubstC[C, A, B](c, CSubst[A, B, C](cr, rop, v)))))
      case (cr, rop, v, x, SubstC(c, sb)) =>
        SubstFm[C, A, B](FactFm[C, A, B](Inst[B, C, A](x, SubstC[C, A, B](c, sb))),
          CSubst[A, B, C](cr, rop, v))
      case (cr, rop, v, x, SomeC(r, c)) =>
        FactFm[C, A, B](Inst[B, C, A](x, SomeC[C, A, B](r, SubstC[C, A, B](c, CSubst[A, B, C](cr, rop, v)))))
      case (cr, rop, v, x, AllC(r, c)) =>
        FactFm[C, A, B](Inst[B, C, A](x, AllC[C, A, B](r, SubstC[C, A, B](c, CSubst[A, B, C](cr, rop, v)))))
    }

  def push_csubst_fact[A: equal, B, C](ca: A, rop: subst_op, v: B, x3: fact[C, A, B]): form[C, A, B] =
    (ca, rop, v, x3) match {
      case (ca, rop, v, Inst(x, c)) =>
        push_csubst_concept[A, B, C](ca, rop, v, x, c)
      case (c, rop, v, AtomR(sign, r, x, y)) =>
        FactFm[C, A, B](AtomR[C, B, A](sign, r, x, y))
      case (c, rop, v, Eq(sign, x, y)) => FactFm[C, A, B](Eq[B, C, A](sign, x, y))
    }

  def push_subst_fact[A: equal, B: equal, C](fct: fact[A, B, C], x1: subst[A, B, C]): form[A, B, C] =
    (fct, x1) match {
      case (fct, RSubst(r, rop, p)) => push_rsubst_fact[A, C, B](r, rop, p, fct)
      case (fct, CSubst(c, rop, v)) => push_csubst_fact[B, C, A](c, rop, v, fct)
    }

  def push_subst_form[A: equal, B: equal, C](x0: form[A, B, C], sbsts: List[subst[A, B, C]]): form[A, B, C] =
    (x0, sbsts) match {
      case (ConstFm(cn), sbsts) => ConstFm[A, B, C](cn)
      case (FactFm(fct), sbsts) =>
        (extract_subst[A, B, C](fct) match {
          case None =>
            (sbsts match {
              case Nil => FactFm[A, B, C](fct)
              case sb :: a =>
                push_subst_form[A, B, C](push_subst_fact[A, B, C](fct, sb), a)
            })
          case Some((x, (c, sb))) =>
            push_subst_form[A, B, C](FactFm[A, B, C](Inst[C, A, B](x, c)), sb :: sbsts)
        })
      case (NegFm(f), sbsts) => NegFm[A, B, C](push_subst_form[A, B, C](f, sbsts))
      case (BinopFm(bop, f1, f2), sbsts) =>
        BinopFm[A, B, C](bop, push_subst_form[A, B, C](f1, sbsts),
          push_subst_form[A, B, C](f2, sbsts))
      case (SubstFm(f, sb), sbsts) => push_subst_form[A, B, C](f, sb :: sbsts)
    }

  def lift_bound_substs[A]: (List[rename[vara[A]]]) => List[rename[vara[A]]] =
    ((a: List[rename[vara[A]]]) =>
      map[rename[vara[A]], rename[vara[A]]](((aa: rename[vara[A]]) =>
        {
          val (ISubst(x, y)): rename[vara[A]] = aa;
          ISubst[vara[A]](lift_var[A](zero_nat, x),
            lift_var[A](zero_nat, y))
        }),
        a))

  def rename_in_qform[A, B, C: equal](x0: qform[A, B, C],
    sbsts: List[rename[vara[C]]]): qform[A, B, C] =
    (x0, sbsts) match {
      case (QConstFm(cn), sbsts) => QConstFm[A, B, C](cn)
      case (QFactFm(fct), sbsts) =>
        QFactFm[A, B, C](rename_in_fact[A, B, vara[C]](fct, sbsts))
      case (QNegFm(f), sbsts) => QNegFm[A, B, C](rename_in_qform[A, B, C](f, sbsts))
      case (QBinopFm(bop, f1, f2), sbsts) =>
        QBinopFm[A, B, C](bop, rename_in_qform[A, B, C](f1, sbsts),
          rename_in_qform[A, B, C](f2, sbsts))
      case (QSubstFm(f, sb), sbsts) =>
        QSubstFm[A, B, C](rename_in_qform[A, B, C](f, sbsts),
          rename_in_subst[A, B, vara[C]](sb, sbsts))
      case (QRenameFm(f, sb), sbsts) => rename_in_qform[A, B, C](f, sb :: sbsts)
      case (QuantifFm(q, v, f), sbsts) =>
        QuantifFm[C, A, B](q, v,
          rename_in_qform[A, B, C](f, lift_bound_substs[C].apply(sbsts)))
    }

  def fv_subst_list[A, B, C: equal](x0: subst[A, B, C]): List[C] = x0 match {
    case RSubst(r, rop, (v1, v2)) => union[C].apply(List(v1)).apply(List(v2))
    case CSubst(c, rop, v) => List(v)
  }

  def fv_concept_list[A, B, C: equal](x0: concept[A, B, C]): List[C] = x0 match {
    case Bottom() => Nil
    case Top() => Nil
    case AtomC(sign, a) => Nil
    case BinopC(bop, c1, c2) =>
      union[C].apply(fv_concept_list[A, B, C](c1)).apply(fv_concept_list[A, B, C](c2))
    case NegC(c) => fv_concept_list[A, B, C](c)
    case NumRestrC(nro, n, r, c) => fv_concept_list[A, B, C](c)
    case SubstC(c, sb) =>
      union[C].apply(fv_concept_list[A, B, C](c)).apply(fv_subst_list[A, B, C](sb))
    case SomeC(r, c) => fv_concept_list[A, B, C](c)
    case AllC(r, c) => fv_concept_list[A, B, C](c)
  }

  def fv_fact_list[A, B, C: equal](x0: fact[A, B, C]): List[C] = x0 match {
    case Inst(x, c) => insert[C](x, fv_concept_list[A, B, C](c))
    case AtomR(sign, r, x, y) => insert[C](x, List(y))
    case Eq(sign, x, y) => insert[C](x, List(y))
  }

  def fv_form_list[A, B, C: equal](x0: form[A, B, C]): List[C] = x0 match {
    case ConstFm(cn) => Nil
    case FactFm(fct) => fv_fact_list[A, B, C](fct)
    case NegFm(f) => fv_form_list[A, B, C](f)
    case BinopFm(bop, f1, f2) =>
      list_union[C](fv_form_list[A, B, C](f1), fv_form_list[A, B, C](f2))
    case SubstFm(f, sb) =>
      list_union[C](fv_form_list[A, B, C](f), fv_subst_list[A, B, C](sb))
  }

  def name_of_free[A](x0: vara[A]): A = x0 match {
    case Free(v) => v
  }

  def close_quantif[A](x0: vara[A]): vara[A] = x0 match {
    case Free(v) => Free[A](v)
    case Bound(n) => Bound[A](minus_nat(n, one_nat))
  }

  def form_of_qform[A, B, C](x0: qform[A, B, C]): form[A, B, C] = x0 match {
    case QConstFm(cn) => ConstFm[A, B, C](cn)
    case QFactFm(fct) =>
      FactFm[A, B, C](apply_var_fact[vara[C], C, A, B](((a: vara[C]) => name_of_free[C](a)), fct))
    case QNegFm(f) => NegFm[A, B, C](form_of_qform[A, B, C](f))
    case QBinopFm(bop, f1, f2) =>
      BinopFm[A, B, C](bop, form_of_qform[A, B, C](f1), form_of_qform[A, B, C](f2))
    case QSubstFm(f, sb) =>
      SubstFm[A, B, C](form_of_qform[A, B, C](f),
        apply_var_subst[vara[C], C, A, B](((a: vara[C]) => name_of_free[C](a)), sb))
  }

  def fv_qform_list[A, B, C: equal](x0: qform[A, B, C]): List[vara[C]] = x0 match {
    case QConstFm(cn) => Nil
    case QFactFm(fct) => fv_fact_list[A, B, vara[C]](fct)
    case QNegFm(f) => fv_qform_list[A, B, C](f)
    case QBinopFm(bop, f1, f2) =>
      list_union[vara[C]](fv_qform_list[A, B, C](f1), fv_qform_list[A, B, C](f2))
    case QuantifFm(q, v, f) =>
      map[vara[C], vara[C]](((a: vara[C]) => close_quantif[C](a)),
        list_remove[vara[C]](fv_qform_list[A, B, C](f),
          List(Bound[C](zero_nat))))
    case QSubstFm(f, sb) =>
      list_union[vara[C]](fv_qform_list[A, B, C](f),
        fv_subst_list[A, B, vara[C]](sb))
    case QRenameFm(f, sb) =>
      {
        val (ISubst(v1, v2)): rename[vara[C]] = sb;
        list_union[vara[C]](list_remove[vara[C]](fv_qform_list[A, B, C](f),
          List(v1)),
          List(v2))
      }
  }

  def powerset[A](x0: List[A]): List[List[A]] = x0 match {
    case Nil => List(Nil)
    case x :: xs => {
      val p: List[List[A]] = powerset[A](xs);
      map[List[A], List[A]](((a: List[A]) => x :: a), p) ++ p
    }
  }

  def size_list[A]: (List[A]) => nat =
    ((a: List[A]) => gen_length[A](zero_nat, a))

  def nnms_to_substs[A](nnms: List[A]): List[rename[vara[A]]] =
    map[(nat, A), rename[vara[A]]](((a: (nat, A)) =>
      {
        val (n, vn): (nat, A) = a;
        ISubst[vara[A]](Bound[A](n), Free[A](vn))
      }),
      zip[nat, A](upt(zero_nat, size_list[A].apply(nnms)),
        nnms))

  def all_forms[A, B, C](br: branch[A, B, C]): List[form[A, B, C]] =
    {
      val (Branch((n, (alf, (asf, (ap, Inactive_form((ico,
        (iac, (iar, (ie, icl))))))))))): branch[A, B, C] = br;
      n ++ (alf ++ (asf ++ (ap ++ (ico ++ (iac ++ (iar ++ (ie ++ icl)))))))
    }

  def fv_branch[A, B, C: equal](br: branch[A, B, C]): List[C] =
    {
      val a: List[form[A, B, C]] = all_forms[A, B, C](br);
      foldl[List[C], form[A, B, C]](((res: List[C]) => (vs: form[A, B, C]) =>
        fv_form_list[A, B, C](vs) ++ res),
        Nil, a)
    }

  def n_subsets[A](n: nat, xs: List[A]): List[List[A]] =
    filter[List[A]](((ys: List[A]) => equal_nat(size_list[A].apply(ys), n)),
      powerset[A](xs))

  def is_falsefm[A, B, C](f: form[A, B, C]): Boolean =
    (f match {
      case ConstFm(true) => false
      case ConstFm(false) => true
      case FactFm(_) => false
      case NegFm(_) => false
      case BinopFm(_, _, _) => false
      case SubstFm(_, _) => false
    })

  def outgoing_rel_from_list[A: equal, B, C: equal](lst: List[form[A, B, C]], x: C, r: A): List[C] =
    remdups[C](foldl[List[C], form[A, B, C]](((res: List[C]) => (a: form[A, B, C]) =>
      (a match {
        case ConstFm(_) => res
        case FactFm(Inst(_, _)) => res
        case FactFm(AtomR(sign, ra, xa, y)) =>
          (if (sign &&
            (eq[C](x, xa) && eq[A](r, ra)))
            y :: res else res)
        case FactFm(Eq(_, _, _)) => res
        case NegFm(_) => res
        case BinopFm(_, _, _) => res
        case SubstFm(_, _) => res
      })),
      Nil, lst))

  def outgoing_rel_from_branch[A: equal, B, C: equal](br: branch[A, B, C], x: C, r: A): List[C] =
    {
      val (Branch((_, (_, (_, (_, Inactive_form((_, (_, (iar, (_, _))))))))))): branch[A, B, C] = br;
      outgoing_rel_from_list[A, B, C](iar, x, r)
    }

  def having_class_list[A: equal, B: equal, C: equal](lst: List[form[A, B, C]], x: C,
    c: concept[A, B, C]): Boolean =
    list_ex[form[A, B, C]](((a: form[A, B, C]) =>
      (a match {
        case ConstFm(_) => false
        case FactFm(Inst(xa, ca)) =>
          eq[C](x, xa) && equal_concept[A, B, C](c, ca)
        case FactFm(AtomR(_, _, _, _)) => false
        case FactFm(Eq(_, _, _)) => false
        case NegFm(_) => false
        case BinopFm(_, _, _) => false
        case SubstFm(_, _) => false
      })),
      lst)

  def having_class_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], x: C,
    c: concept[A, B, C]): Boolean =
    {
      val (Branch((_, (alf, (asf, (_, Inactive_form((ico,
        (iac, (_, (_, _))))))))))): branch[A, B, C] = br;
      having_class_list[A, B, C](alf ++ (asf ++ (ico ++ iac)), x, c)
    }

  def unequal_in_list[A: equal, B: equal, C: equal](lst: List[form[A, B, C]], x1: C, x2: C): Boolean =
    member[form[A, B, C]](lst, FactFm[A, B, C](Eq[C, A, B](false, x1, x2))) ||
      member[form[A, B, C]](lst, FactFm[A, B, C](Eq[C, A, B](false, x2, x1)))

  def unequal_in_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], x1: C, x2: C): Boolean =
    {
      val (Branch((_, (_, (_, (_, Inactive_form((_, (_, (_, (ie, _))))))))))): branch[A, B, C] = br;
      unequal_in_list[A, B, C](ie, x1, x2)
    }

  def set_of_pairs[A: ord](xs: List[A]): List[(A, A)] =
    maps[A, (A, A)](((x1: A) =>
      maps[A, (A, A)](((x2: A) =>
        (if (less[A](x1, x2)) List((x1, x2))
        else Nil)),
        xs)),
      xs)

  def numrestrc_lt_applicable_vars_branch[A: equal, B: equal, C: equal: ord](br: branch[A, B, C], x: C, n: nat, r: A, c: concept[A, B, C]): List[(C, C)] =
    {
      val ys_outgoing: List[C] =
        remdups[C](outgoing_rel_from_branch[A, B, C](br, x, r))
      val ys_c: List[C] =
        filter[C](((y: C) => having_class_branch[A, B, C](br, y, c)),
          ys_outgoing);
      (if (less_nat(size_list[C].apply(ys_c), n)) Nil
      else {
        val a: List[(C, C)] = set_of_pairs[C](ys_c);
        filter[(C, C)](((aa: (C, C)) =>
          {
            val (x1, x2): (C, C) = aa;
            !(unequal_in_branch[A, B, C](br, x1, x2))
          }),
          a)
      })
    }

  def numrestrc_lt_applicable_branch[A: equal, B: equal, C: equal: ord](br: branch[A, B, C], x: C,
    n: nat, r: A, c: concept[A, B, C]): Boolean =
    !(nulla[(C, C)](numrestrc_lt_applicable_vars_branch[A, B, C](br, x, n, r, c)))

  def choose_applicable_vars_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], x: C, r: A,
    c: concept[A, B, C]): (List[C], List[C]) =
    {
      val ys_outgoing: List[C] =
        remdups[C](outgoing_rel_from_branch[A, B, C](br, x, r))
      val ys_c: List[C] =
        filter[C](((y: C) => having_class_branch[A, B, C](br, y, c)), ys_outgoing)
      val not_c: concept[A, B, C] = neg_norm_concept[A, B, C](false, c)
      val ys_not_c: List[C] =
        filter[C](((y: C) => having_class_branch[A, B, C](br, y, not_c)),
          ys_outgoing)
      val ys_undet: List[C] =
        filter[C](((y: C) =>
          !(member[C](ys_c, y)) && !(member[C](ys_not_c, y))),
          ys_outgoing)
      val a: List[C] = list_inters[C](ys_c, ys_not_c);
      (ys_undet, a)
    }

  def choose_applicable_in_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], x: C, r: A,
    c: concept[A, B, C]): Boolean =
    !(nulla[C](fst[List[C], List[C]](choose_applicable_vars_branch[A, B, C](br, x, r, c))))

  def is_applicable_permanent[A: equal, B: equal, C: equal: ord](br: branch[A, B, C],
    f: form[A, B, C]): Boolean =
    (f match {
      case ConstFm(_) => false
      case FactFm(Inst(_, Top())) => false
      case FactFm(Inst(_, Bottom())) => false
      case FactFm(Inst(_, AtomC(_, _))) => false
      case FactFm(Inst(_, NegC(_))) => false
      case FactFm(Inst(_, BinopC(_, _, _))) => false
      case FactFm(Inst(x, NumRestrC(Lt(), n, r, c))) =>
        choose_applicable_in_branch[A, B, C](br, x, r, c) ||
          numrestrc_lt_applicable_branch[A, B, C](br, x, n, r, c)
      case FactFm(Inst(_, NumRestrC(Ge(), _, _, _))) => false
      case FactFm(Inst(_, SubstC(_, _))) => false
      case FactFm(Inst(_, SomeC(_, _))) => false
      case FactFm(Inst(_, AllC(_, _))) => false
      case FactFm(AtomR(_, _, _, _)) => false
      case FactFm(Eq(_, _, _)) => false
      case NegFm(_) => false
      case BinopFm(_, _, _) => false
      case SubstFm(_, _) => false
    })

  def is_inactive[A: equal, B: equal, C: equal: ord](br: branch[A, B, C]): Boolean =
    (br match {
      case Branch((Nil, (Nil, (Nil, (ap, _))))) =>
        pred_list[form[A, B, C]](((f: form[A, B, C]) =>
          !(is_applicable_permanent[A, B, C](br, f))),
          ap)
      case Branch((Nil, (Nil, (_ :: _, _)))) => false
      case Branch((Nil, (_ :: _, _))) => false
      case Branch((_ :: _, _)) => false
    })

  def is_neg_role[A: equal, B: equal, C: equal, D](f: form[A, B, C], fn: form[A, D, C]): Boolean =
    (fn match {
      case ConstFm(_) => false
      case FactFm(Inst(_, _)) => false
      case FactFm(AtomR(sign, r, x, y)) =>
        equal_forma[A, B, C](f, FactFm[A, B, C](AtomR[A, C, B](!sign, r, x, y)))
      case FactFm(Eq(_, _, _)) => false
      case NegFm(_) => false
      case BinopFm(_, _, _) => false
      case SubstFm(_, _) => false
    })

  def failed_subproof[A, B, C](current: trace[A, B, C],
    x1: List[trace_constr[A, B, C]]): Option[List[trace_constr[A, B, C]]] =
    (current, x1) match {
      case (current, Nil) =>
        Some[List[trace_constr[A, B, C]]](List(CTrace[A, B, C](current)))
      case (current, ITrace(subtrs, n, rr) :: traces) =>
        (if (equal_nat(plus_nat(one_nat, size_list[trace[A, B, C]].apply(subtrs)),
          n))
          failed_subproof[A, B, C](Trace[A, B, C](rr, rev[trace[A, B, C]](current :: subtrs)),
          traces)
        else Some[List[trace_constr[A, B, C]]](ITrace[A, B, C](current :: subtrs, n, rr) ::
          traces))
      case (current, CTrace(vb) :: va) => None
    }

  def add_new_form[A, B, C](f: form[A, B, C], br: branch[A, B, C]): branch[A, B, C] =
    {
      val (Branch((n, (alf, (asf, (ap, ia)))))): branch[A, B, C] = br;
      Branch[A, B, C]((f :: n, (alf, (asf, (ap, ia)))))
    }

  def mutually_distinct_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], xs: List[C]): Boolean =
    pred_list[C](((x1: C) =>
      pred_list[C](((x2: C) =>
        eq[C](x1, x2) ||
          unequal_in_branch[A, B, C](br, x1, x2)),
        xs)),
      xs)

  def exist_outgoing_r_c_distincts_from_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], x: C, n: nat, r: A, c: concept[A, B, C]): Boolean =
    {
      val ys_outgoing: List[C] = outgoing_rel_from_branch[A, B, C](br, x, r)
      val ys_c: List[C] =
        filter[C](((y: C) => having_class_branch[A, B, C](br, y, c)), ys_outgoing)
      val a: List[List[C]] = n_subsets[C](n, ys_c);
      list_ex[List[C]](((aa: List[C]) =>
        mutually_distinct_branch[A, B, C](br, aa)),
        a)
    }

  def numrestrc_ge_applicable_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], x: C,
    n: nat, r: A, c: concept[A, B, C]): Boolean =
    !(exist_outgoing_r_c_distincts_from_branch[A, B, C](br, x, n, r, c))

  def make_outgoing_r_c_from[A, B, C](x: A, r: B, c: concept[B, C, A], nvrs: List[A]): List[form[B, C, A]] =
    map[A, form[B, C, A]](((y: A) => FactFm[B, C, A](AtomR[B, A, C](true, r, x, y))),
      nvrs) ++
      map[A, form[B, C, A]](((y: A) => FactFm[B, C, A](Inst[A, B, C](y, c))), nvrs)

  def make_mutually_distinct[A: ord, B, C](nvrs: List[A]): List[form[B, C, A]] =
    map[(A, A), form[B, C, A]](((a: (A, A)) => {
      val (x, y): (A, A) = a;
      FactFm[B, C, A](Eq[A, B, C](false, x, y))
    }),
      set_of_pairs[A](nvrs))

  def new_vars_list[A: new_var_list_class](n: nat, vns: List[A], vn: A): List[A] =
    (if (equal_nat(n, zero_nat)) Nil
    else {
      val nvar: A = new_var_list[A](vns, vn);
      nvar :: new_vars_list[A](minus_nat(n, one_nat), nvar :: vns, vn)
    })

  def numrestrc_ge_generated[A, B, C: equal: new_var_list_class: ord](br: branch[A, B, C],
    x: C, n: nat, r: A, c: concept[A, B, C]): List[form[A, B, C]] =
    {
      val vrs: List[C] = fv_branch[A, B, C](br)
      val nvrs: List[C] = new_vars_list[C](n, vrs, x);
      make_mutually_distinct[C, A, B](nvrs) ++
        make_outgoing_r_c_from[C, A, B](x, r, c, nvrs)
    }

  def bag_insert_front[A: equal](x: A, xs: List[A]): List[A] =
    (if (member[A](xs, x)) xs else x :: xs)

  def add_inactive_composite[A: equal, B: equal, C: equal](f: form[A, B, C], br: branch[A, B, C]): branch[A, B, C] =
    {
      val (Branch((n, (alf, (asf, (ap, Inactive_form((ico,
        (iac, (iar, (ie, icl))))))))))): branch[A, B, C] = br;
      Branch[A, B, C]((n, (alf, (asf, (ap, Inactive_form[A, B, C]((bag_insert_front[form[A, B, C]](f, ico),
        (iac, (iar, (ie, icl))))))))))
    }

  def add_new_forms[A, B, C](fs: List[form[A, B, C]], br: branch[A, B, C]): branch[A, B, C] =
    {
      val (Branch((n, (alf, (asf, (ap, ia)))))): branch[A, B, C] = br;
      Branch[A, B, C]((fs ++ n, (alf, (asf, (ap, ia)))))
    }

  def apply_numrestrc_ge_branch[A: equal, B: equal, C: equal: new_var_list_class: ord](br: branch[A, B, C],
    f: form[A, B, C], x: C, n: nat, r: A,
    c: concept[A, B, C]): apply_result[A, B, C] =
    (if (equal_nat(n, zero_nat))
      AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
    else (if (numrestrc_ge_applicable_branch[A, B, C](br, x, n, r, c))
      AppRes[A, B, C](one_nat,
      Some[rule_rep[A, B, C]](NumrestrC_GeRule_rep[A, B, C](f)),
      List(add_new_forms[A, B, C](numrestrc_ge_generated[A, B, C](br, x, n, r, c),
        add_inactive_composite[A, B, C](f, br))))
    else AppRes[A, B, C](zero_nat, None,
      List(add_inactive_composite[A, B, C](f, br)))))

  def conjfm_applicable_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], f1: form[A, B, C],
    f2: form[A, B, C]): Boolean =
    {
      val fs: List[form[A, B, C]] = all_forms[A, B, C](br);
      !(member[form[A, B, C]](fs, f1) && member[form[A, B, C]](fs, f2))
    }

  def apply_conjfm_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], f: form[A, B, C],
    f1: form[A, B, C], f2: form[A, B, C]): apply_result[A, B, C] =
    (if (conjfm_applicable_branch[A, B, C](br, f1, f2))
      AppRes[A, B, C](one_nat, Some[rule_rep[A, B, C]](ConjFmRule_rep[A, B, C](f)),
      List(add_new_forms[A, B, C](List(f2, f1),
        add_inactive_composite[A, B, C](f, br))))
    else AppRes[A, B, C](zero_nat, None,
      List(add_inactive_composite[A, B, C](f, br))))

  def propagate_equality[A: equal, B: equal, C: equal](x: A, y: A, xs: List[form[B, C, A]]): List[form[B, C, A]] =
    remdups[form[B, C, A]](map[form[B, C, A], form[B, C, A]](((frm: form[B, C, A]) =>
      rename_in_form[B, C, A](frm, List(ISubst[A](x, y)))),
      xs))

  def conjc_applicable_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], x: C,
    c1: concept[A, B, C], c2: concept[A, B, C]): Boolean =
    {
      val fs: List[form[A, B, C]] = all_forms[A, B, C](br);
      !(member[form[A, B, C]](fs, FactFm[A, B, C](Inst[C, A, B](x, c1))) &&
        member[form[A, B, C]](fs, FactFm[A, B, C](Inst[C, A, B](x, c2))))
    }

  def apply_conjc_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], f: form[A, B, C], x: C,
    c1: concept[A, B, C], c2: concept[A, B, C]): apply_result[A, B, C] =
    (if (conjc_applicable_branch[A, B, C](br, x, c1, c2))
      AppRes[A, B, C](one_nat, Some[rule_rep[A, B, C]](ConjCRule_rep[A, B, C](f)),
      List(add_new_forms[A, B, C](List(FactFm[A, B, C](Inst[C, A, B](x, c2)),
        FactFm[A, B, C](Inst[C, A, B](x, c1))),
        add_inactive_composite[A, B, C](f, br))))
    else AppRes[A, B, C](zero_nat, None,
      List(add_inactive_composite[A, B, C](f, br))))

  def reactivate_equivs[A, B, C](br: branch[A, B, C]): branch[A, B, C] =
    {
      val (Branch((n, (alf, (asf, (ap, Inactive_form((ico,
        (iac, (iar, (ie, icl))))))))))): branch[A, B, C] = br;
      Branch[A, B, C]((n ++ ie,
        (alf, (asf, (ap, Inactive_form[A, B, C]((ico, (iac, (iar, (Nil, icl))))))))))
    }

  def inactive_form_apply[A, B, C, D, E, F](f: (List[form[A, B, C]]) => List[form[D, E, F]],
    iaf: inactive_form[A, B, C]): inactive_form[D, E, F] =
    {
      val (Inactive_form((ico, (iac, (iar, (ie, icl)))))): inactive_form[A, B, C] = iaf;
      Inactive_form[D, E, F]((f(ico), (f(iac), (f(iar), (f(ie), f(icl))))))
    }

  def branch_apply[A, B, C, D, E, F](f: (List[form[A, B, C]]) => List[form[D, E, F]],
    br: branch[A, B, C]): branch[D, E, F] =
    {
      val (Branch((n, (alf, (asf, (ap, ia)))))): branch[A, B, C] = br;
      Branch[D, E, F]((f(n),
        (f(alf),
          (f(asf),
            (f(ap), inactive_form_apply[A, B, C, D, E, F](f, ia))))))
    }

  def apply_linear[A: equal, B: equal, C: equal: new_var_list_class: ord](br: branch[A, B, C],
    f: form[A, B, C]): apply_result[A, B, C] =
    (f match {
      case ConstFm(_) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, Top())) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, Bottom())) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, AtomC(_, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, NegC(_))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(x, BinopC(Conj(), c1, c2))) =>
        apply_conjc_branch[A, B, C](br, f, x, c1, c2)
      case FactFm(Inst(_, BinopC(Disj(), _, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, NumRestrC(Lt(), _, _, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(x, NumRestrC(Ge(), n, r, c))) =>
        apply_numrestrc_ge_branch[A, B, C](br, f, x, n, r, c)
      case FactFm(Inst(_, SubstC(_, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, SomeC(_, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, AllC(_, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(AtomR(_, _, _, _)) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Eq(true, x, y)) =>
        AppRes[A, B, C](one_nat, Some[rule_rep[A, B, C]](EqRule_rep[A, B, C](f)),
          List(reactivate_equivs[A, B, C](branch_apply[A, B, C, A, B, C](((a: List[form[A, B, C]]) =>
            propagate_equality[C, A, B](x, y, a)),
            add_inactive_composite[A, B, C](f, br)))))
      case FactFm(Eq(false, _, _)) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case NegFm(_) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case BinopFm(Conj(), f1, f2) => apply_conjfm_branch[A, B, C](br, f, f1, f2)
      case BinopFm(Disj(), _, _) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case SubstFm(_, _) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
    })

  def all_forms_not_new[A, B, C](br: branch[A, B, C]): List[form[A, B, C]] =
    {
      val (Branch((_, (alf, (asf, (ap, Inactive_form((ico,
        (iac, (iar, (ie, icl))))))))))): branch[A, B, C] = br;
      alf ++ (asf ++ (ap ++ (ico ++ (iac ++ (iar ++ (ie ++ icl))))))
    }

  def add_active_splitting[A: equal, B: equal, C: equal](f: form[A, B, C], br: branch[A, B, C]): branch[A, B, C] =
    {
      val (Branch((n, (alf, (asf, (ap, ia)))))): branch[A, B, C] = br;
      Branch[A, B, C]((n, (alf, (bag_insert_front[form[A, B, C]](f, asf), (ap, ia)))))
    }

  def add_inactive_clash[A: equal, B: equal, C: equal](f: form[A, B, C], br: branch[A, B, C]): branch[A, B, C] =
    {
      val (Branch((n, (alf, (asf, (ap, Inactive_form((ico,
        (iac, (iar, (ie, icl))))))))))): branch[A, B, C] = br;
      Branch[A, B, C]((n, (alf, (asf, (ap, Inactive_form[A, B, C]((ico, (iac, (iar, (ie, bag_insert_front[form[A, B, C]](f, icl)))))))))))
    }

  def add_active_linear[A: equal, B: equal, C: equal](f: form[A, B, C], br: branch[A, B, C]): branch[A, B, C] =
    {
      val (Branch((n, (alf, (asf, (ap, ia)))))): branch[A, B, C] = br;
      Branch[A, B, C]((n, (bag_insert_front[form[A, B, C]](f, alf), (asf, (ap, ia)))))
    }

  def add_inactive_equivs[A: equal, B: equal, C: equal](f: form[A, B, C], br: branch[A, B, C]): branch[A, B, C] =
    {
      val (Branch((n, (alf, (asf, (ap, Inactive_form((ico,
        (iac, (iar, (ie, icl))))))))))): branch[A, B, C] = br;
      Branch[A, B, C]((n, (alf, (asf, (ap, Inactive_form[A, B, C]((ico, (iac, (iar, (bag_insert_front[form[A, B, C]](f, ie),
        icl))))))))))
    }

  def add_inactive_atomr[A: equal, B: equal, C: equal](f: form[A, B, C], br: branch[A, B, C]): branch[A, B, C] =
    {
      val (Branch((n, (alf, (asf, (ap, Inactive_form((ico,
        (iac, (iar, (ie, icl))))))))))): branch[A, B, C] = br;
      Branch[A, B, C]((n, (alf, (asf, (ap, Inactive_form[A, B, C]((ico, (iac, (bag_insert_front[form[A, B, C]](f, iar),
        (ie, icl))))))))))
    }

  def bag_insert_end[A: equal](x: A, xs: List[A]): List[A] =
    (if (member[A](xs, x)) xs else xs ++ List(x))

  def add_active_permanent[A: equal, B: equal, C: equal](f: form[A, B, C], br: branch[A, B, C]): branch[A, B, C] =
    {
      val (Branch((n, (alf, (asf, (ap, ia)))))): branch[A, B, C] = br;
      Branch[A, B, C]((n, (alf, (asf, (bag_insert_end[form[A, B, C]](f, ap), ia)))))
    }

  def add_inactive_atomc[A: equal, B: equal, C: equal](f: form[A, B, C], br: branch[A, B, C]): branch[A, B, C] =
    {
      val (Branch((n, (alf, (asf, (ap, Inactive_form((ico,
        (iac, (iar, (ie, icl))))))))))): branch[A, B, C] = br;
      Branch[A, B, C]((n, (alf, (asf, (ap, Inactive_form[A, B, C]((ico, (bag_insert_front[form[A, B, C]](f, iac),
        (iar, (ie, icl))))))))))
    }

  def classify_concept[A: equal, B: equal, C: equal](br: branch[A, B, C], f: form[A, B, C]): branch[A, B, C] =
    (f match {
      case FactFm(Inst(_, Top())) => add_inactive_composite[A, B, C](f, br)
      case FactFm(Inst(_, Bottom())) => add_inactive_clash[A, B, C](f, br)
      case FactFm(Inst(_, AtomC(_, _))) => add_inactive_atomc[A, B, C](f, br)
      case FactFm(Inst(_, NegC(_))) => add_inactive_composite[A, B, C](f, br)
      case FactFm(Inst(_, BinopC(Conj(), _, _))) =>
        add_active_linear[A, B, C](f, br)
      case FactFm(Inst(_, BinopC(Disj(), _, _))) =>
        add_active_splitting[A, B, C](f, br)
      case FactFm(Inst(_, NumRestrC(Lt(), _, _, _))) =>
        add_active_permanent[A, B, C](f, br)
      case FactFm(Inst(_, NumRestrC(Ge(), _, _, _))) =>
        add_active_linear[A, B, C](f, br)
      case FactFm(Inst(_, SubstC(_, _))) => add_active_splitting[A, B, C](f, br)
      case FactFm(Inst(_, SomeC(_, _))) => add_active_linear[A, B, C](f, br)
      case FactFm(Inst(_, AllC(_, _))) => add_active_permanent[A, B, C](f, br)
    })

  def classify_fact[A: equal, B: equal, C: equal](br: branch[A, B, C], f: form[A, B, C]): branch[A, B, C] =
    (f match {
      case FactFm(Inst(_, _)) => classify_concept[A, B, C](br, f)
      case FactFm(AtomR(_, _, _, _)) => add_inactive_atomr[A, B, C](f, br)
      case FactFm(Eq(true, x, y)) =>
        (if (eq[C](x, y)) add_inactive_composite[A, B, C](f, br)
        else add_active_linear[A, B, C](f, br))
      case FactFm(Eq(false, x, y)) =>
        (if (eq[C](x, y)) add_inactive_clash[A, B, C](f, br)
        else add_inactive_equivs[A, B, C](f, br))
    })

  def classify_form[A: equal, B: equal, C: equal](br: branch[A, B, C], f: form[A, B, C]): branch[A, B, C] =
    (f match {
      case ConstFm(true) => add_inactive_composite[A, B, C](f, br)
      case ConstFm(false) => add_inactive_clash[A, B, C](f, br)
      case FactFm(_) => classify_fact[A, B, C](br, f)
      case NegFm(fa) => add_inactive_composite[A, B, C](fa, br)
      case BinopFm(Conj(), _, _) => add_active_linear[A, B, C](f, br)
      case BinopFm(Disj(), _, _) => add_active_splitting[A, B, C](f, br)
      case SubstFm(_, _) => add_active_splitting[A, B, C](f, br)
    })

  def classify_new[A: equal, B: equal, C: equal](br: branch[A, B, C]): branch[A, B, C] =
    (br match {
      case Branch((Nil, (_, (_, (_, _))))) => br
      case Branch((nf :: nfs, (alf, (asf, (ap, ia))))) =>
        (if (member[form[A, B, C]](all_forms_not_new[A, B, C](br), nf))
          classify_new[A, B, C](Branch[A, B, C]((nfs, (alf, (asf, (ap, ia))))))
        else classify_form[A, B, C](Branch[A, B, C]((nfs, (alf, (asf, (ap, ia))))),
          nf))
    })

  def is_ineq_inst[A, B, C: equal](f: form[A, B, C]): Boolean =
    (f match {
      case ConstFm(_) => false
      case FactFm(Inst(_, _)) => false
      case FactFm(AtomR(_, _, _, _)) => false
      case FactFm(Eq(true, _, _)) => false
      case FactFm(Eq(false, x, y)) => eq[C](x, y)
      case NegFm(_) => false
      case BinopFm(_, _, _) => false
      case SubstFm(_, _) => false
    })

  def qform_to_plform[A, B, C](x0: qform[A, B, C]): plform[A, B, C] = x0 match {
    case QConstFm(b) => PlConstFm[A, B, C](b)
  }

  def map_classify_branches[A: equal, B: equal, C: equal](ar: apply_result[A, B, C]): apply_result[A, B, C] =
    {
      val (AppRes(n, rr_opt, brs)): apply_result[A, B, C] = ar;
      AppRes[A, B, C](n, rr_opt,
        map[branch[A, B, C], branch[A, B, C]](((a: branch[A, B, C]) =>
          classify_new[A, B, C](a)),
          brs))
    }

  def substfm_applicable_branch[A: equal, B: equal, C: equal](br: branch[A, B, C],
    fp: form[A, B, C]): Boolean =
    {
      val fs: List[form[A, B, C]] = all_forms[A, B, C](br);
      !(member[form[A, B, C]](fs, fp))
    }

  def apply_substfm_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], f: form[A, B, C]): apply_result[A, B, C] =
    {
      val fp: form[A, B, C] =
        neg_norm_form[A, B, C](true, push_subst_form[A, B, C](f, Nil));
      (if (substfm_applicable_branch[A, B, C](br, fp))
        AppRes[A, B, C](one_nat, Some[rule_rep[A, B, C]](SubstFmRule_rep[A, B, C](f)),
        List(add_new_form[A, B, C](fp,
          add_inactive_composite[A, B, C](f, br))))
      else AppRes[A, B, C](zero_nat, None,
        List(add_inactive_composite[A, B, C](f, br))))
    }

  def substc_applicable_branch[A: equal, B: equal, C: equal](br: branch[A, B, C],
    fp: form[A, B, C]): Boolean =
    {
      val fs: List[form[A, B, C]] = all_forms[A, B, C](br);
      !(member[form[A, B, C]](fs, fp))
    }

  def apply_substc_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], f: form[A, B, C]): apply_result[A, B, C] =
    {
      val fp: form[A, B, C] =
        neg_norm_form[A, B, C](true, push_subst_form[A, B, C](f, Nil));
      (if (substc_applicable_branch[A, B, C](br, fp))
        AppRes[A, B, C](one_nat, Some[rule_rep[A, B, C]](SubstCRule_rep[A, B, C](f)),
        List(add_new_form[A, B, C](fp,
          add_inactive_composite[A, B, C](f, br))))
      else AppRes[A, B, C](zero_nat, None,
        List(add_inactive_composite[A, B, C](f, br))))
    }

  def disjfm_applicable_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], f1: form[A, B, C],
    f2: form[A, B, C]): Boolean =
    {
      val fs: List[form[A, B, C]] = all_forms[A, B, C](br);
      !(member[form[A, B, C]](fs, f1)) && !(member[form[A, B, C]](fs, f2))
    }

  def apply_disjfm_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], f: form[A, B, C],
    f1: form[A, B, C], f2: form[A, B, C]): apply_result[A, B, C] =
    (if (disjfm_applicable_branch[A, B, C](br, f1, f2))
      AppRes[A, B, C](nat_of_integer(BigInt(2)),
      Some[rule_rep[A, B, C]](DisjFmRule_rep[A, B, C](f)),
      List(add_new_form[A, B, C](f1, add_inactive_composite[A, B, C](f, br)),
        add_new_form[A, B, C](f2,
          add_inactive_composite[A, B, C](f, br))))
    else AppRes[A, B, C](zero_nat, None,
      List(add_inactive_composite[A, B, C](f, br))))

  def disjc_applicable_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], x: C,
    c1: concept[A, B, C], c2: concept[A, B, C]): Boolean =
    {
      val fs: List[form[A, B, C]] = all_forms[A, B, C](br);
      !(member[form[A, B, C]](fs, FactFm[A, B, C](Inst[C, A, B](x, c1)))) &&
        !(member[form[A, B, C]](fs, FactFm[A, B, C](Inst[C, A, B](x, c2))))
    }

  def apply_disjc_branch[A: equal, B: equal, C: equal](br: branch[A, B, C], f: form[A, B, C], x: C,
    c1: concept[A, B, C], c2: concept[A, B, C]): apply_result[A, B, C] =
    (if (disjc_applicable_branch[A, B, C](br, x, c1, c2))
      AppRes[A, B, C](nat_of_integer(BigInt(2)),
      Some[rule_rep[A, B, C]](DisjCRule_rep[A, B, C](f)),
      List(add_new_form[A, B, C](FactFm[A, B, C](Inst[C, A, B](x, c1)),
        add_inactive_composite[A, B, C](f, br)),
        add_new_form[A, B, C](FactFm[A, B, C](Inst[C, A, B](x, c2)),
          add_inactive_composite[A, B, C](f, br))))
    else AppRes[A, B, C](zero_nat, None,
      List(add_inactive_composite[A, B, C](f, br))))

  def apply_splitting[A: equal, B: equal, C: equal](br: branch[A, B, C], f: form[A, B, C]): apply_result[A, B, C] =
    (f match {
      case ConstFm(_) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, Top())) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, Bottom())) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, AtomC(_, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, NegC(_))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, BinopC(Conj(), _, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(x, BinopC(Disj(), c1, c2))) =>
        apply_disjc_branch[A, B, C](br, f, x, c1, c2)
      case FactFm(Inst(_, NumRestrC(_, _, _, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, SubstC(_, _))) => apply_substc_branch[A, B, C](br, f)
      case FactFm(Inst(_, SomeC(_, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, AllC(_, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(AtomR(_, _, _, _)) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Eq(_, _, _)) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case NegFm(_) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case BinopFm(Conj(), _, _) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case BinopFm(Disj(), f1, f2) => apply_disjfm_branch[A, B, C](br, f, f1, f2)
      case SubstFm(_, _) => apply_substfm_branch[A, B, C](br, f)
    })

  def apply_permanent[A: equal, B: equal, C: equal: ord](br: branch[A, B, C], f: form[A, B, C]): apply_result[A, B, C] =
    (f match {
      case ConstFm(_) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, Top())) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, Bottom())) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, AtomC(_, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, NegC(_))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, BinopC(_, _, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(x, NumRestrC(Lt(), n, r, c))) =>
        (numrestrc_lt_applicable_vars_branch[A, B, C](br, x, n, r, c) match {
          case Nil =>
            (choose_applicable_vars_branch[A, B, C](br, x, r, c) match {
              case (Nil, Nil) =>
                AppRes[A, B, C](zero_nat, None,
                  List(add_active_permanent[A, B, C](f, br)))
              case (Nil, _ :: _) =>
                AppRes[A, B, C](one_nat,
                  Some[rule_rep[A, B, C]](Todo_rep[A, B, C]()),
                  List(add_inactive_clash[A, B, C](ConstFm[A, B, C](false), br)))
              case (y :: _, _) =>
                AppRes[A, B, C](nat_of_integer(BigInt(2)),
                  Some[rule_rep[A, B, C]](ChooseRule_rep[A, B, C](f)),
                  List(add_new_form[A, B, C](FactFm[A, B, C](Inst[C, A, B](y, c)),
                    add_active_permanent[A, B, C](f, br)),
                    add_new_form[A, B, C](FactFm[A, B, C](Inst[C, A, B](y, neg_norm_concept[A, B, C](false, c))),
                      add_active_permanent[A, B, C](f, br))))
            })
          case a :: list =>
            AppRes[A, B, C](size_list[(C, C)].apply(a :: list),
              Some[rule_rep[A, B, C]](Todo_rep[A, B, C]()),
              map[(C, C), branch[A, B, C]](((aa: (C, C)) =>
                {
                  val (xa, y): (C, C) = aa;
                  add_new_form[A, B, C](FactFm[A, B, C](Eq[C, A, B](true, xa, y)),
                    add_active_permanent[A, B, C](f, br))
                }),
                a :: list))
        })
      case FactFm(Inst(_, NumRestrC(Ge(), _, _, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, SubstC(_, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, SomeC(_, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Inst(_, AllC(_, _))) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(AtomR(_, _, _, _)) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case FactFm(Eq(_, _, _)) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case NegFm(_) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case BinopFm(_, _, _) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
      case SubstFm(_, _) =>
        AppRes[A, B, C](zero_nat, None, List(add_inactive_composite[A, B, C](f, br)))
    })

  def apply_rules_pt[A: equal, B: equal, C: equal: new_var_list_class: ord](x0: branch[A, B, C]): apply_result[A, B, C] =
    x0 match {
      case Branch((Nil, (Nil, (Nil, (Nil, ia))))) =>
        AppRes[A, B, C](zero_nat, None,
          List(Branch[A, B, C]((Nil, (Nil, (Nil, (Nil, ia)))))))
      case Branch((Nil, (Nil, (Nil, (apf :: apfs, ia))))) =>
        map_classify_branches[A, B, C](apply_permanent[A, B, C](Branch[A, B, C]((Nil, (Nil, (Nil, (apfs, ia))))), apf))
      case Branch((Nil, (Nil, (asf :: asfs, (ap, ia))))) =>
        map_classify_branches[A, B, C](apply_splitting[A, B, C](Branch[A, B, C]((Nil, (Nil, (asfs, (ap, ia))))), asf))
      case Branch((Nil, (alf :: alfs, (asf, (ap, ia))))) =>
        map_classify_branches[A, B, C](apply_linear[A, B, C](Branch[A, B, C]((Nil, (alfs, (asf, (ap, ia))))), alf))
      case Branch((vc :: vd, vb)) =>
        AppRes[A, B, C](zero_nat, None,
          List(classify_new[A, B, C](Branch[A, B, C]((vc :: vd, vb)))))
    }

  def is_neg_concept[A: equal, B: equal, C: equal, D](f: form[A, B, C], fn: form[D, B, C]): Boolean =
    (fn match {
      case ConstFm(_) => false
      case FactFm(a) =>
        (a match {
          case Inst(x, aa) =>
            (aa match {
              case Top() => false
              case Bottom() => false
              case AtomC(sign, ab) =>
                equal_forma[A, B, C](f, FactFm[A, B, C](Inst[C, A, B](x, AtomC[B, A, C](!sign, ab))))
              case NegC(_) => false
              case BinopC(_, _, _) => false
              case NumRestrC(_, _, _, _) => false
              case SubstC(_, _) => false
              case SomeC(_, _) => false
              case AllC(_, _) => false
            })
          case AtomR(_, _, _, _) => false
          case Eq(_, _, _) => false
        })
      case NegFm(_) => false
      case BinopFm(_, _, _) => false
      case SubstFm(_, _) => false
    })

  def is_inactive_string(br: branch[String, String, String]): Boolean =
    is_inactive[String, String, String](br)

  def rename_closure_qform[A, B, C](fm: qform[A, B, C], x1: List[rename[vara[C]]]): qform[A, B, C] =
    (fm, x1) match {
      case (fm, Nil) => fm
      case (fm, sb :: sbsts) =>
        rename_closure_qform[A, B, C](QRenameFm[A, B, C](fm, sb), sbsts)
    }

  def free_univ_bound_list[A, B, C: new_var_list_class](x0: qform[A, B, C],
    nnms: List[C], fvs: List[C]): qform[A, B, C] =
    (x0, nnms, fvs) match {
      case (QuantifFm(QAll(), v, f), nnms, fvs) =>
        {
          val nv_name: C = new_var_list[C](nnms ++ fvs, v);
          free_univ_bound_list[A, B, C](f, nv_name :: nnms, fvs)
        }
      case (QConstFm(v), nnms, fvs) =>
        rename_closure_qform[A, B, C](QConstFm[A, B, C](v), nnms_to_substs[C](nnms))
      case (QFactFm(v), nnms, fvs) =>
        rename_closure_qform[A, B, C](QFactFm[A, B, C](v), nnms_to_substs[C](nnms))
      case (QNegFm(v), nnms, fvs) =>
        rename_closure_qform[A, B, C](QNegFm[A, B, C](v), nnms_to_substs[C](nnms))
      case (QBinopFm(v, va, vb), nnms, fvs) =>
        rename_closure_qform[A, B, C](QBinopFm[A, B, C](v, va, vb),
          nnms_to_substs[C](nnms))
      case (QuantifFm(QEx(), va, vb), nnms, fvs) =>
        rename_closure_qform[A, B, C](QuantifFm[C, A, B](QEx(), va, vb),
          nnms_to_substs[C](nnms))
      case (QSubstFm(v, va), nnms, fvs) =>
        rename_closure_qform[A, B, C](QSubstFm[A, B, C](v, va), nnms_to_substs[C](nnms))
      case (QRenameFm(v, va), nnms, fvs) =>
        rename_closure_qform[A, B, C](QRenameFm[A, B, C](v, va), nnms_to_substs[C](nnms))
    }

  def free_prenex_form_list[A, B, C: equal: new_var_list_class](f: qform[A, B, C]): form[A, B, C] =
    form_of_qform[A, B, C](rename_in_qform[A, B, C](free_univ_bound_list[A, B, C](lift_bound[A, B, C](f), Nil,
      map[vara[C], C](((a: vara[C]) => name_of_free[C](a)),
        fv_qform_list[A, B, C](f))),
      Nil))

  def free_prenex_form_list_string[A, B]: (qform[A, B, String]) => form[A, B, String] =
    ((a: qform[A, B, String]) => free_prenex_form_list[A, B, String](a))

  def init_tableau_triple(pre: form[String, String, String],
    c: stmt[String, String, String],
    post: form[String, String, String]): List[form[String, String, String]] =
    {
      val vcs: qform[String, String, String] =
        extract_vcs[String, String, String](qform_of_form[String, String, String](pre), c,
          qform_of_form[String, String, String](post))
      val neg_prenex_f: form[String, String, String] =
        neg_norm_form[String, String, String](false,
          free_prenex_form_list_string[String, String].apply(vcs));
      List(neg_prenex_f)
    }

  def is_atomic_role_fact[A, B, C](s: Boolean, fn: form[A, B, C]): Boolean =
    (fn match {
      case ConstFm(_) => false
      case FactFm(Inst(_, _)) => false
      case FactFm(AtomR(sign, _, _, _)) => equal_bool(s, sign)
      case FactFm(Eq(_, _, _)) => false
      case NegFm(_) => false
      case BinopFm(_, _, _) => false
      case SubstFm(_, _) => false
    })

  def role_of_atomic_fact[A, B, C](fn: form[A, B, C]): A =
    {
      val (FactFm(AtomR(_, r, _, _))): form[A, B, C] = fn;
      r
    }

  def insts_of_atomic_role[A, B, C](fn: form[A, B, C]): (C, C) =
    {
      val (FactFm(a)): form[A, B, C] = fn
      val (AtomR(_, _, aa, b)): fact[A, B, C] = a;
      (aa, b)
    }

  def contains_numrestrc_clash_branch[A: equal, B: equal, C: equal](br: branch[A, B, C]): Boolean =
    {
      val (Branch((_, (_, (_, (ap, _)))))): branch[A, B, C] = br;
      list_ex[form[A, B, C]](((a: form[A, B, C]) =>
        (a match {
          case ConstFm(_) => false
          case FactFm(Inst(_, Top())) => false
          case FactFm(Inst(_, Bottom())) => false
          case FactFm(Inst(_, AtomC(_, _))) => false
          case FactFm(Inst(_, NegC(_))) => false
          case FactFm(Inst(_, BinopC(_, _, _))) => false
          case FactFm(Inst(x, NumRestrC(Lt(), n, r, c))) =>
            (if (equal_nat(n, zero_nat)) true
            else exist_outgoing_r_c_distincts_from_branch[A, B, C](br, x, n, r, c))
          case FactFm(Inst(_, NumRestrC(Ge(), _, _, _))) =>
            false
          case FactFm(Inst(_, SubstC(_, _))) => false
          case FactFm(Inst(_, SomeC(_, _))) => false
          case FactFm(Inst(_, AllC(_, _))) => false
          case FactFm(AtomR(_, _, _, _)) => false
          case FactFm(Eq(_, _, _)) => false
          case NegFm(_) => false
          case BinopFm(_, _, _) => false
          case SubstFm(_, _) => false
        })),
        ap)
    }

  def contains_contr_concept_list[A: equal, B: equal, C: equal](lst: List[form[A, B, C]]): Boolean =
    list_ex[form[A, B, C]](((f: form[A, B, C]) =>
      list_ex[form[A, B, C]](((a: form[A, B, C]) =>
        is_neg_concept[A, B, C, A](f, a)),
        lst)),
      lst)

  def contains_contr_concept_branch[A: equal, B: equal, C: equal](br: branch[A, B, C]): Boolean =
    {
      val (Branch((_, (_, (_, (_, Inactive_form((_, (iac, (_, (_, _))))))))))): branch[A, B, C] = br;
      contains_contr_concept_list[A, B, C](iac)
    }

  def contains_contr_role_list[A: equal, B: equal, C: equal](lst: List[form[A, B, C]]): Boolean =
    list_ex[form[A, B, C]](((f: form[A, B, C]) =>
      list_ex[form[A, B, C]](((a: form[A, B, C]) =>
        is_neg_role[A, B, C, B](f, a)),
        lst)),
      lst)

  def contains_contr_role_branch[A: equal, B: equal, C: equal](br: branch[A, B, C]): Boolean =
    {
      val (Branch((_, (_, (_, (_, Inactive_form((_, (_, (iar, (_, _))))))))))): branch[A, B, C] = br;
      contains_contr_role_list[A, B, C](iar)
    }

  def nonempty_clashset_branch[A, B, C](br: branch[A, B, C]): Boolean =
    {
      val (Branch((_, (_, (_, (_, Inactive_form((_, (_, (_, (_, icl))))))))))): branch[A, B, C] = br;
      !(nulla[form[A, B, C]](icl))
    }

  def contains_contr_eq_list[A, B, C: equal](lst: List[form[A, B, C]]): Boolean =
    list_ex[form[A, B, C]](((a: form[A, B, C]) => is_ineq_inst[A, B, C](a)), lst)

  def contains_contr_eq_branch[A, B, C: equal](br: branch[A, B, C]): Boolean =
    {
      val (Branch(a)): branch[A, B, C] = br
      val (_, aa): (List[form[A, B, C]], (List[form[A, B, C]], (List[form[A, B, C]], (List[form[A, B, C]], inactive_form[A, B, C])))) = a
      val (_, ab): (List[form[A, B, C]], (List[form[A, B, C]], (List[form[A, B, C]], inactive_form[A, B, C]))) = aa
      val (_, ac): (List[form[A, B, C]], (List[form[A, B, C]], inactive_form[A, B, C])) = ab
      val (_, ad): (List[form[A, B, C]], inactive_form[A, B, C]) = ac
      val (Inactive_form(ae)): inactive_form[A, B, C] = ad
      val (_, af): (List[form[A, B, C]], (List[form[A, B, C]], (List[form[A, B, C]], (List[form[A, B, C]], List[form[A, B, C]])))) = ae
      val (_, ag): (List[form[A, B, C]], (List[form[A, B, C]], (List[form[A, B, C]], List[form[A, B, C]]))) = af
      val (_, ah): (List[form[A, B, C]], (List[form[A, B, C]], List[form[A, B, C]])) = ag
      val (_, ai): (List[form[A, B, C]], List[form[A, B, C]]) = ah;
      contains_contr_eq_list[A, B, C](ai)
    }

  def contains_falsefm_list[A, B, C](lst: List[form[A, B, C]]): Boolean =
    list_ex[form[A, B, C]](((a: form[A, B, C]) => is_falsefm[A, B, C](a)), lst)

  def contains_falsefm_branch[A, B, C](br: branch[A, B, C]): Boolean =
    {
      val (Branch(a)): branch[A, B, C] = br
      val (_, aa): (List[form[A, B, C]], (List[form[A, B, C]], (List[form[A, B, C]], (List[form[A, B, C]], inactive_form[A, B, C])))) = a
      val (_, ab): (List[form[A, B, C]], (List[form[A, B, C]], (List[form[A, B, C]], inactive_form[A, B, C]))) = aa
      val (_, ac): (List[form[A, B, C]], (List[form[A, B, C]], inactive_form[A, B, C])) = ab
      val (_, ad): (List[form[A, B, C]], inactive_form[A, B, C]) = ac
      val (Inactive_form(ae)): inactive_form[A, B, C] = ad
      val (_, af): (List[form[A, B, C]], (List[form[A, B, C]], (List[form[A, B, C]], (List[form[A, B, C]], List[form[A, B, C]])))) = ae
      val (_, ag): (List[form[A, B, C]], (List[form[A, B, C]], (List[form[A, B, C]], List[form[A, B, C]]))) = af
      val (_, ah): (List[form[A, B, C]], (List[form[A, B, C]], List[form[A, B, C]])) = ag
      val (_, ai): (List[form[A, B, C]], List[form[A, B, C]]) = ah;
      contains_falsefm_list[A, B, C](ai)
    }

  def contains_clash_branch[A: equal, B: equal, C: equal](br: branch[A, B, C]): Boolean =
    (br match {
      case Branch((Nil, (_, (_, (_, _))))) =>
        nonempty_clashset_branch[A, B, C](br) ||
          (contains_contr_concept_branch[A, B, C](br) ||
            (contains_contr_role_branch[A, B, C](br) ||
              (contains_contr_eq_branch[A, B, C](br) ||
                (contains_falsefm_branch[A, B, C](br) ||
                  contains_numrestrc_clash_branch[A, B, C](br)))))
      case Branch((_ :: _, _)) => false
    })

  def group_by_funct[A: equal, B: equal](f: A => B, x1: List[A]): B => List[A] =
    (f, x1) match {
      case (f, Nil) => ((_: B) => Nil)
      case (f, x :: xs) => {
        val g: B => List[A] = group_by_funct[A, B](f, xs);
        fun_upd[B, List[A]](g, f(x), insert[A](x, g(f(x))))
      }
    }

  def concept_of_atomic_fact[A, B, C](fn: form[A, B, C]): B =
    {
      val (FactFm(a)): form[A, B, C] = fn
      val (Inst(_, aa)): fact[A, B, C] = a
      val (AtomC(_, ab)): concept[A, B, C] = aa;
      ab
    }

  def inst_of_atomic_concept[A, B, C](fn: form[A, B, C]): C =
    {
      val (FactFm(Inst(x, AtomC(_, _)))): form[A, B, C] = fn;
      x
    }

  def is_atomic_concept_fact[A, B, C](s: Boolean, fn: form[A, B, C]): Boolean =
    (fn match {
      case ConstFm(_) => false
      case FactFm(Inst(_, Top())) => false
      case FactFm(Inst(_, Bottom())) => false
      case FactFm(Inst(_, AtomC(sign, _))) => equal_bool(s, sign)
      case FactFm(Inst(_, NegC(_))) => false
      case FactFm(Inst(_, BinopC(_, _, _))) => false
      case FactFm(Inst(_, NumRestrC(_, _, _, _))) => false
      case FactFm(Inst(_, SubstC(_, _))) => false
      case FactFm(Inst(_, SomeC(_, _))) => false
      case FactFm(Inst(_, AllC(_, _))) => false
      case FactFm(AtomR(_, _, _, _)) => false
      case FactFm(Eq(_, _, _)) => false
      case NegFm(_) => false
      case BinopFm(_, _, _) => false
      case SubstFm(_, _) => false
    })

  def concept_of_atomic_fact_signed[A, B, C](fn: form[A, B, C]): (Boolean, B) =
    {
      val (FactFm(a)): form[A, B, C] = fn
      val (Inst(_, aa)): fact[A, B, C] = a
      val (AtomC(ab, b)): concept[A, B, C] = aa;
      (ab, b)
    }

  def interp_atomic_inst_conc_impl[A: equal, B: equal, C: equal](ab_i: List[form[A, B, C]]): C => List[(Boolean, B)] =
    comp[List[form[A, B, C]], List[(Boolean, B)], C](((a: List[form[A, B, C]]) =>
      map[form[A, B, C], (Boolean, B)](((aa: form[A, B, C]) =>
        concept_of_atomic_fact_signed[A, B, C](aa)),
        a)),
      group_by_funct[form[A, B, C], C](((a: form[A, B, C]) =>
        inst_of_atomic_concept[A, B, C](a)),
        filter[form[A, B, C]](((a: form[A, B, C]) => is_atomic_concept_fact[A, B, C](true, a)),
          ab_i) ++
          filter[form[A, B, C]](((a: form[A, B, C]) => is_atomic_concept_fact[A, B, C](false, a)),
            ab_i)))

  def interp_atomic_concept_impl[A: equal, B: equal, C: equal](ab_i: List[form[A, B, C]]): B => List[C] =
    comp[List[form[A, B, C]], List[C], B](((a: List[form[A, B, C]]) =>
      map[form[A, B, C], C](((aa: form[A, B, C]) =>
        inst_of_atomic_concept[A, B, C](aa)),
        a)),
      group_by_funct[form[A, B, C], B](((a: form[A, B, C]) =>
        concept_of_atomic_fact[A, B, C](a)),
        filter[form[A, B, C]](((a: form[A, B, C]) => is_atomic_concept_fact[A, B, C](true, a)),
          ab_i)))

  def interp_atomic_role_impl[A: equal, B: equal, C: equal](ab_i: List[form[A, B, C]]): A => List[(C, C)] =
    comp[List[form[A, B, C]], List[(C, C)], A](((a: List[form[A, B, C]]) =>
      map[form[A, B, C], (C, C)](((aa: form[A, B, C]) =>
        insts_of_atomic_role[A, B, C](aa)),
        a)),
      group_by_funct[form[A, B, C], A](((a: form[A, B, C]) =>
        role_of_atomic_fact[A, B, C](a)),
        filter[form[A, B, C]](((a: form[A, B, C]) => is_atomic_role_fact[A, B, C](true, a)), ab_i)))

  def abox_impl_concept_list[A, B: equal, C](ab_i: List[form[A, B, C]]): List[B] =
    remdups[B](map_filter[form[A, B, C], B](((x: form[A, B, C]) =>
      (if (is_atomic_concept_fact[A, B, C](true, x))
        Some[B](concept_of_atomic_fact[A, B, C](x))
      else None)),
      ab_i))

  def abox_impl_role_list[A: equal, B, C](ab_i: List[form[A, B, C]]): List[A] =
    remdups[A](map_filter[form[A, B, C], A](((x: form[A, B, C]) =>
      (if (is_atomic_role_fact[A, B, C](true, x))
        Some[A](role_of_atomic_fact[A, B, C](x))
      else None)),
      ab_i))

  def fv_abox_impl_list[A, B, C: equal](ab_i: List[form[A, B, C]]): List[C] =
    list_Union[C](map[form[A, B, C], List[C]](((a: form[A, B, C]) => fv_form_list[A, B, C](a)),
      ab_i))

  def canon_interp_impl[A: equal, B: equal, C: equal](ab_i: List[form[A, B, C]]): Interp_impl_ext[A, B, C, Unit] =
    Interp_impl_exta[C, B, A, Unit](fv_abox_impl_list[A, B, C](ab_i),
      abox_impl_concept_list[A, B, C](ab_i),
      abox_impl_role_list[A, B, C](ab_i),
      interp_atomic_concept_impl[A, B, C](ab_i),
      interp_atomic_role_impl[A, B, C](ab_i),
      interp_atomic_inst_conc_impl[A, B, C](ab_i), ())

  def interp_impl_c[A, B, C, D](x0: Interp_impl_ext[A, B, C, D]): List[B] = x0 match {
    case Interp_impl_exta(interp_impl_i, interp_impl_c, interp_impl_r,
      interp_impl_c_i, interp_impl_r_i, interp_impl_i_c,
      more) => interp_impl_c
  }

  def interp_impl_i[A, B, C, D](x0: Interp_impl_ext[A, B, C, D]): List[C] = x0 match {
    case Interp_impl_exta(interp_impl_i, interp_impl_c, interp_impl_r,
      interp_impl_c_i, interp_impl_r_i, interp_impl_i_c,
      more) => interp_impl_i
  }

  def interp_impl_r[A, B, C, D](x0: Interp_impl_ext[A, B, C, D]): List[A] = x0 match {
    case Interp_impl_exta(interp_impl_i, interp_impl_c, interp_impl_r,
      interp_impl_c_i, interp_impl_r_i, interp_impl_i_c,
      more) => interp_impl_r
  }

  def interp_impl_c_i[A, B, C, D](x0: Interp_impl_ext[A, B, C, D]): B => List[C] =
    x0 match {
      case Interp_impl_exta(interp_impl_i, interp_impl_c, interp_impl_r,
        interp_impl_c_i, interp_impl_r_i, interp_impl_i_c,
        more) => interp_impl_c_i
    }

  def interp_impl_i_c[A, B, C, D](x0: Interp_impl_ext[A, B, C, D]): C => List[(Boolean, B)] =
    x0 match {
      case Interp_impl_exta(interp_impl_i, interp_impl_c, interp_impl_r,
        interp_impl_c_i, interp_impl_r_i, interp_impl_i_c,
        more) => interp_impl_i_c
    }

  def interp_impl_r_i[A, B, C, D](x0: Interp_impl_ext[A, B, C, D]): A => List[(C, C)] =
    x0 match {
      case Interp_impl_exta(interp_impl_i, interp_impl_c, interp_impl_r,
        interp_impl_c_i, interp_impl_r_i, interp_impl_i_c,
        more) => interp_impl_r_i
    }
  
def nth[A](x0: List[A], n: nat): A = (x0, n) match {
   case (x :: xs, n) =>
     (if (equal_nat(n, zero_nat)) x else nth[A](xs, minus_nat(n, one_nat)))
}

} /* object VerifCondition */
